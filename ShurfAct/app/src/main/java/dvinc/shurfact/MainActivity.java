package dvinc.shurfact;

import android.app.LoaderManager;
import android.content.ContentUris;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.app.AlertDialog;
import android.content.DialogInterface;


import android.widget.AdapterView;
import android.widget.ListView;


import dvinc.shurfact.data.ActContract.ActEntry;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener ,LoaderManager.LoaderCallbacks<Cursor>{
    /** Identifier for the act data loader */
    private static final int ACT_LOADER = 0;
    /** Adapter for the ListView */
    ActCursorAdapter mCursorAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, CreateActivity.class);
                startActivity(intent);

            }
        });
        // Find the ListView which will be populated with the act data
        ListView actListView = (ListView) findViewById(R.id.list);

        // Find and set empty view on the ListView, so that it only shows when the list has 0 items.
        View emptyView = findViewById(R.id.empty_view);
        actListView.setEmptyView(emptyView);



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        // Setup an Adapter to create a list item for each row of act data in the Cursor.
        // There is no act data yet (until the loader finishes) so pass in null for the Cursor.
        mCursorAdapter = new ActCursorAdapter(this, null);
        actListView.setAdapter(mCursorAdapter);

        //Установка итем клик листенера
        actListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                // Create new intent to go to {@link EditorActivity}
                Intent intent = new Intent(MainActivity.this, CreateActivity.class);

                // Form the content URI that represents the specific act that was clicked on,
                // by appending the "id" (passed as input to this method) onto the
                // {@link ActEntry#CONTENT_URI}.
                // For example, the URI would be "content://com.example.android.acts/acts/2"
                // if the act with ID 2 was clicked on.
                Uri currentActUri = ContentUris.withAppendedId(ActEntry.CONTENT_URI, id);

                // Set the URI on the data field of the intent
                intent.setData(currentActUri);

                // Launch the {@link EditorActivity} to display the data for the current act.
                startActivity(intent);
            }
        });

        // Kick off the loader
        getLoaderManager().initLoader(ACT_LOADER, null, this);
    }

    private void showDeleteALLConfirmationDialog() {
        AlertDialog.Builder builder2 = new AlertDialog.Builder(this);
        builder2.setMessage(R.string.delete_all_dialog_msg);
        builder2.setPositiveButton(R.string.delete_all, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                deleteAllActs();
            }
        });
        builder2.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });

        AlertDialog alertDialog = builder2.create();
        alertDialog.show();
    }



    /**
     * Helper method to delete all acts in the database.
     */
    private void deleteAllActs() {
        int rowsDeleted = getContentResolver().delete(ActEntry.CONTENT_URI, null, null);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu options from the res/menu/menu_catalog.xml file.
        // This adds menu items to the app bar.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // User clicked on a menu option in the app bar overflow menu
        switch (item.getItemId()) {
            // Respond to a click on the "Insert dummy data" menu option
            case R.id.action_insert_dummy_data:
                //insertAct();
                return true;
            // Respond to a click on the "Delete all entries" menu option
            //case R.id.action_delete_all_entries:
            // Do nothing for now
            //  return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.edit_db) {
            showDeleteALLConfirmationDialog();
        } else if (id == R.id.nav_gallery) {
            Intent intent = new Intent(MainActivity.this, AboutActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        // Define a projection that specifies the columns from the table we care about.
        String[] projection = {
                ActEntry._ID,     //ИД важно для любого курсорАдаптера
                ActEntry.COLUMN_1_NUBM,
                ActEntry.COLUMN_1_DATE};

        // This loader will execute the ContentProvider's query method on a background thread

        return new CursorLoader(this,   // Parent activity context
                ActEntry.CONTENT_URI,   // Provider content URI to query
                projection,             // Columns to include in the resulting Cursor
                null,                   // No selection clause
                null,                   // No selection arguments
                null);                  // Default sort order
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        // Update {@link ActCursorAdapter} with this new cursor containing updated act data
        mCursorAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        // Callback called when the data needs to be deleted
        mCursorAdapter.swapCursor(null);
    }
}
