package dvinc.shurfact;


import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.print.PrintAttributes;
import android.print.PrintDocumentAdapter;
import android.print.PrintJob;
import android.print.PrintManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;



/**
 * Created by Space 5 on 04.12.2016.
 */

public class WebActivity extends AppCompatActivity {
    private WebView mWebView;
    private PrintJob mPrintJobs;
    private static String FILE = Environment.getExternalStorageDirectory()
            .getAbsolutePath() + "/filename.pdf";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);
        String htmlText = getIntent().getExtras().getString("htmltext");
        mWebView = (WebView) findViewById(R.id.webView);
        // включаем поддержку JavaScript
        mWebView.getSettings().setJavaScriptEnabled(true);
        // указываем страницу загрузки
        mWebView.loadDataWithBaseURL("file:///android_asset/", htmlText, "text/html", "en_US", null);
        mWebView.setInitialScale(1);
        mWebView.getSettings().setLoadWithOverviewMode(true);
        mWebView.getSettings().setUseWideViewPort(true);
        mWebView.getSettings().setBuiltInZoomControls(true);
        mWebView.getSettings().setDisplayZoomControls(false);

    }

    private void createWebPrintJob(WebView webView) {

            PrintManager printManager = (PrintManager) this.getSystemService(Context.PRINT_SERVICE);

            PrintDocumentAdapter printAdapter = webView.createPrintDocumentAdapter();

            String jobName = "Print Test";

            printManager.print(jobName, printAdapter, new PrintAttributes.Builder()
                    .setMediaSize(PrintAttributes.MediaSize.ISO_A3)
                    .setColorMode(PrintAttributes.COLOR_MODE_COLOR)
                    .build());


    }

    public void onClick(View view) {

        createWebPrintJob(mWebView);
    }

    public void mailClick(View view) {
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        // The intent does not have a URI, so declare the "text/plain" MIME type
        emailIntent.setType("message/rfc822");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[] {"the_dude@ultramail.com"}); // recipients
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Let me show a magic");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Hello, take and look this");
        emailIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://"+FILE));
        // You can also attach multiple items by passing an ArrayList of Uris
        emailIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // this will make such that when user returns to your app, your app is displayed, instead of the email app.
        startActivity(emailIntent);
    }
}
