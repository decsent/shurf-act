package dvinc.shurfact;

/**
 * Created by Space 5 on 29.11.2016.
 */
import android.Manifest;
import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.app.LoaderManager;
import android.content.ContentValues;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Loader;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import dvinc.shurfact.data.ActContract.ActEntry;

public class CreateActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    /**
     * Identifier for the act data loader
     */
    private static final int EXISTING_ACT_LOADER = 0;

    /**
     * Content URI for the existing act (null if it's a new act)
     */
    private Uri mCurrentActUri;

    protected EditText mActNumb;
    protected EditText mDate;
    protected EditText mObjectName;
    protected EditText mCoordPlace;
    protected Spinner mLspinner;

    protected TextView tvLocationGPS2;
    protected TextView tvEnabledGPS;
    protected TextView tvStatusGPS;
    protected TextView tvLocationGPS;
    protected LocationManager locationManager;

    protected Spinner mDspinner;
    protected EditText mWallThickness;
    protected Spinner mRspinner;
    protected Spinner mDTspinner;

    protected String mLspin = ActEntry.EMPTY_FIELD;
    protected String mDspin = ActEntry.EMPTY_FIELD;
    protected String mRspin = ActEntry.EMPTY_FIELD;
    protected String mDTspin = ActEntry.EMPTY_FIELD;
    protected String actdate = " ";


    protected EditText mGroundDepth;
    protected Spinner mGTspinner;
    protected Spinner mGTgspinner;
    protected EditText mGroundResist;
    protected Spinner mGCspinner;
    protected String mGTspin = ActEntry.EMPTY_FIELD;
    protected String mGTgspin = ActEntry.EMPTY_FIELD;
    protected String mGCspin = ActEntry.EMPTY_FIELD;
    protected LinearLayout LLGroundGruntBlock;


    protected EditText mProtecThickness;
    protected EditText mDamageArea;
    protected Spinner mPMspinner;
    protected Spinner mPAspinner;
    protected Spinner mPCspinner;
    protected Spinner mADspinner;
    protected Spinner mDTyspinner;
    protected Spinner mDAspinner;
    protected String mPMspin = ActEntry.EMPTY_FIELD;
    protected String mPAspin = ActEntry.EMPTY_FIELD;
    protected String mPCspin = ActEntry.EMPTY_FIELD;
    protected String mADspin = ActEntry.EMPTY_FIELD;
    protected String mDTyspin = ActEntry.EMPTY_FIELD;
    protected String mDAspin = ActEntry.EMPTY_FIELD;
    protected LinearLayout LLProtecConditBlock;
    protected LinearLayout LL_DAsquareblock;

    protected Spinner mCDspinner;
    protected Spinner mCDCspinner;
    protected Spinner mCDMspinner;
    protected Spinner mCD1spinner;
    protected Spinner mCD13spinner;
    protected Spinner mCD3spinner;
    protected String mCDspin = ActEntry.EMPTY_FIELD;
    protected String mCDCspin = ActEntry.EMPTY_FIELD;
    protected String mCDMspin = ActEntry.EMPTY_FIELD;
    protected String mCD1spin = ActEntry.EMPTY_FIELD;
    protected String mCD13spin = ActEntry.EMPTY_FIELD;
    protected String mCD3spin = ActEntry.EMPTY_FIELD;
    protected LinearLayout LL_CDCblock;
    protected LinearLayout LL_CD1block;
    protected LinearLayout LL_CD13block;
    protected LinearLayout LL_CD3block;


    protected EditText mOtherPoten;
    protected Spinner mOMspinner;
    protected String mOMspin = ActEntry.EMPTY_FIELD;

    protected ImageView ImageView;
    protected EditText mPhotoComment;
    protected String image64;

    protected Spinner mOrg1spinner;
    protected Spinner mOrg2spinner;
    protected String mOrg1spin = ActEntry.EMPTY_FIELD;
    protected String mOrg2spin = ActEntry.EMPTY_FIELD;
    protected EditText mOrgUser;



    /**
     * Boolean flag that keeps track of whether the act has been edited (true) or not (false)
     */
    private boolean mActHasChanged = false;

    /**
     * OnTouchListener that listens for any user touches on a View, implying that they are modifying
     * the view, and we change the mActHasChanged boolean to true.
     */
    private View.OnTouchListener mTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            mActHasChanged = true;
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_test);

        // Find all relevant views that we will need to read user input from
        mActNumb = (EditText) findViewById(R.id.edit_1_numb);
        mDate = (EditText) findViewById(R.id.txtdate);
        mObjectName = (EditText) findViewById(R.id.edit_1_object_name);
        mCoordPlace = (EditText) findViewById(R.id.edit_1_coord_place);
        mWallThickness = (EditText) findViewById(R.id.edit_1_thickness);

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        final Button mgpsButton = (Button) findViewById(R.id.gpsButton);
        tvEnabledGPS = (TextView) findViewById(R.id.tvEnabledGPS);
        tvStatusGPS = (TextView) findViewById(R.id.tvStatusGPS);
        tvLocationGPS = (TextView) findViewById(R.id.tvLocationGPS);
        tvLocationGPS2 = (TextView) findViewById(R.id.tvLocationGPS2);

        mLspinner = (Spinner) findViewById(R.id.spinner_lenght);
        mDspinner = (Spinner) findViewById(R.id.spinner_diameter);
        mRspinner = (Spinner) findViewById(R.id.spinner_reason);
        mDTspinner = (Spinner) findViewById(R.id.spinner_terrain);

        mGroundDepth = (EditText) findViewById(R.id.ground_depth);
        mGTspinner = (Spinner) findViewById(R.id.spinner_ground_type);
        mGTgspinner = (Spinner) findViewById(R.id.spinner_ground_grunt);
        LLGroundGruntBlock = (LinearLayout) findViewById(R.id.ground_grunt_block);
        mGroundResist = (EditText) findViewById(R.id.ground_resist);
        mGCspinner = (Spinner) findViewById(R.id.spinner_ground_condit);


        mProtecThickness = (EditText) findViewById(R.id.protec_thicknes);
        mDamageArea = (EditText) findViewById(R.id.damage_area);
        mPMspinner = (Spinner) findViewById(R.id.spinner_protec_mater);
        mPAspinner  = (Spinner) findViewById(R.id.spinner_protec_availab);
        mPCspinner = (Spinner) findViewById(R.id.spinner_protec_condit);
        mADspinner = (Spinner) findViewById(R.id.spinner_adgesion);
        mDTyspinner = (Spinner) findViewById(R.id.spinner_damage_type);
        mDAspinner = (Spinner) findViewById(R.id.spinner_damage_availab);
        LLProtecConditBlock = (LinearLayout) findViewById(R.id.protec_condit_block);

        mCDspinner = (Spinner) findViewById(R.id.spinner_corr_damage);
        mCDCspinner= (Spinner) findViewById(R.id.spinner_corr_dam_char);
        mCDMspinner= (Spinner) findViewById(R.id.spinner_corr_dam_max);
        mCD1spinner = (Spinner) findViewById(R.id.spinner_corr_dam_1);
        mCD13spinner = (Spinner) findViewById(R.id.spinner_corr_dam_1_3);
        mCD3spinner = (Spinner) findViewById(R.id.spinner_corr_dam_3);
        LL_CDCblock = (LinearLayout) findViewById(R.id.corr_dam_char);
        LL_CD1block = (LinearLayout) findViewById(R.id.corr_dam_1);
        LL_CD13block = (LinearLayout) findViewById(R.id.corr_dam_13);
        LL_CD3block = (LinearLayout) findViewById(R.id.corr_dam_3);
        LL_DAsquareblock = (LinearLayout) findViewById(R.id.damage_area_square);

        mOtherPoten = (EditText) findViewById(R.id.other_poten);
        mOMspinner = (Spinner) findViewById(R.id.spinner_other_mater);

        ImageView = (ImageView) findViewById(R.id.imageViewLoad);
        mPhotoComment = (EditText) findViewById(R.id.photo_comment);

        mOrg1spinner = (Spinner) findViewById(R.id.spinner_org1);
        mOrg2spinner = (Spinner) findViewById(R.id.spinner_org2);
        mOrgUser = (EditText) findViewById(R.id.edit_org_user);


        mgpsButton.setTag(1);
        mgpsButton.setText("Получить");
        mgpsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final int status = (Integer) v.getTag();
                if (status == 1) {
                    if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000 * 10, 10, locationListener);
                    checkEnabled1();
                    mgpsButton.setText("Остановить");
                    v.setTag(0); //pause
                } else {
                    locationManager.removeUpdates(locationListener);
                    mgpsButton.setText("Получить");
                    v.setTag(1); //pause
                }
            }


        });

        // Examine the intent that was used to launch this activity,
        // in order to figure out if we're creating a new act or editing an existing one.
        Intent intent = getIntent();
        mCurrentActUri = intent.getData();
        // If the intent DOES NOT contain a act content URI, then we know that we are
        // creating a new act.
        if (mCurrentActUri == null) {
            // This is a new act, so change the app bar to say "Add a Act"
            setTitle(getString(R.string.editor_activity_title_new_act));
            // Invalidate the options menu, so the "Delete" menu option can be hidden.
            // (It doesn't make sense to delete a act that hasn't been created yet.)
            invalidateOptionsMenu();
        } else {
            // Otherwise this is an existing act, so change app bar to say "Edit Act"
            setTitle(getString(R.string.editor_activity_title_edit_act));
            // Initialize a loader to read the act data from the database
            // and display the current values in the editor
            getLoaderManager().initLoader(EXISTING_ACT_LOADER, null, this);
        }

        // Setup OnTouchListeners on all the input fields, so we can determine if the user
        // has touched or modified them. This will let us know if there are unsaved changes
        // or not, if the user tries to leave the editor without saving.
        mActNumb.setOnTouchListener(mTouchListener);
        mDate.setOnTouchListener(mTouchListener);
        mObjectName.setOnTouchListener(mTouchListener);
        mCoordPlace.setOnTouchListener(mTouchListener);
        mWallThickness.setOnTouchListener(mTouchListener);
        mLspinner.setOnTouchListener(mTouchListener);
        mDspinner.setOnTouchListener(mTouchListener);
        mRspinner.setOnTouchListener(mTouchListener);
        mDTspinner.setOnTouchListener(mTouchListener);
        mGroundDepth.setOnTouchListener(mTouchListener);
        mGroundResist.setOnTouchListener(mTouchListener);
        mGTspinner.setOnTouchListener(mTouchListener);
        mGCspinner.setOnTouchListener(mTouchListener);

        setupSpinnerL();
        setupSpinnerD();
        setupSpinnerR();
        setupSpinnerDT();
        setupSpinnerGT();
        setupSpinnerGTg();
        setupSpinnerGC();
        setupSpinnerPM();
        setupSpinnerPA();
        setupSpinnerPC();
        setupSpinnerAD();
        setupSpinnerDTy();
        setupSpinnerDA();
        setupSpinnerCD();
        setupSpinnerCDC();
        setupSpinnerCDM();
        setupSpinnerCD1();
        setupSpinnerCD13();
        setupSpinnerCD3();
        setupSpinnerOM();

        setupSpinnerOrg1();
        setupSpinnerOrg2();

        mDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View view, boolean hasfocus) {
                if (hasfocus) {
                    DateDialog dialog = new DateDialog(view);
                    FragmentTransaction ft = getFragmentManager().beginTransaction();
                    dialog.show(ft, "DatePicker");
                }
            }
        });
    }

    /**
     * Setup the dropdown spinner that allows the user to select the gender of the Act.
     */
    private void setupSpinnerL() {
        // Create adapter for spinner. The list options are from the String array it will use
        // the spinner will use the default layout
        ArrayAdapter lenghtSpinnerAdapter = ArrayAdapter.createFromResource(this,
                R.array.array_lenght_pit, android.R.layout.simple_spinner_item);
        // Specify dropdown layout style - simple list view with 1 item per line
        lenghtSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        // Apply the adapter to the spinner
        mLspinner.setAdapter(lenghtSpinnerAdapter);
        // Set the integer mSelected to the constant values
        mLspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mLspin = (String) parent.getItemAtPosition(position);
                if (mLspin.equals(getString(R.string.not_chosen))) {
                    mLspin = ActEntry.EMPTY_FIELD;
                }
            }
            // Because AdapterView is an abstract class, onNothingSelected must be defined
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                mLspin = ActEntry.EMPTY_FIELD;
            }
        });
    }

    private void setupSpinnerD() {
        ArrayAdapter diamSpinnerAdapter = ArrayAdapter.createFromResource(this,
                R.array.array_diam, android.R.layout.simple_spinner_item);
        diamSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        mDspinner.setAdapter(diamSpinnerAdapter);
        mDspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mDspin = (String) parent.getItemAtPosition(position);
                if (mDspin.equals(getString(R.string.not_chosen))) {
                    mDspin = ActEntry.EMPTY_FIELD;
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                mDspin = ActEntry.EMPTY_FIELD;
            }
        });
    }
    private void setupSpinnerR() {
        ArrayAdapter ReasonSpinnerAdapter = ArrayAdapter.createFromResource(this,
                R.array.array_reason, android.R.layout.simple_spinner_item);
        ReasonSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        mRspinner.setAdapter(ReasonSpinnerAdapter);
        mRspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mRspin = (String) parent.getItemAtPosition(position);
                if (mRspin.equals(getString(R.string.not_chosen))) {
                    mRspin = ActEntry.EMPTY_FIELD;
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                mRspin = ActEntry.EMPTY_FIELD;
            }
        });
    }
    private void setupSpinnerDT() {
        ArrayAdapter descSpinnerAdapter = ArrayAdapter.createFromResource(this,
                R.array.array_desc_terr, android.R.layout.simple_spinner_item);
        descSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        mDTspinner.setAdapter(descSpinnerAdapter);
        mDTspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mDTspin = (String) parent.getItemAtPosition(position);
                if (mDTspin.equals(getString(R.string.not_chosen))) {
                    mDTspin = ActEntry.EMPTY_FIELD;
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                mDTspin = ActEntry.EMPTY_FIELD;
            }
        });
    }
    private void setupSpinnerGT() {
        ArrayAdapter groundtypeSpinnerAdapter = ArrayAdapter.createFromResource(this,
                R.array.array_ground_type, android.R.layout.simple_spinner_item);
        groundtypeSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        mGTspinner.setAdapter(groundtypeSpinnerAdapter);
        mGTspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mGTspin = (String) parent.getItemAtPosition(position);
                    if (mGTspin.equals(getString(R.string.not_chosen))) {
                        mGTspin = ActEntry.EMPTY_FIELD;
                        LLGroundGruntBlock.setVisibility(View.GONE);
                    } else {
                        LLGroundGruntBlock.setVisibility(View.VISIBLE);
                    }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                mGTspin = ActEntry.EMPTY_FIELD;
                LLGroundGruntBlock.setVisibility(View.GONE);
            }
        });
    }
    private void setupSpinnerGTg() {
        ArrayAdapter groundgruntSpinnerAdapter = ArrayAdapter.createFromResource(this,
                R.array.array_ground_grunt, android.R.layout.simple_spinner_item);
        groundgruntSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        mGTgspinner.setAdapter(groundgruntSpinnerAdapter);
        mGTgspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mGTgspin = (String) parent.getItemAtPosition(position);
                    if (mGTgspin.equals(getString(R.string.not_chosen))) {
                        mGTgspin = ActEntry.EMPTY_FIELD;
                    } else if (mGTgspin.equals(getString(R.string.gtg1))) {
                        mGTgspin = ActEntry.GTg_2;
                    } else if (mGTgspin.equals(getString(R.string.gtg2))) {
                        mGTgspin = ActEntry.GTg_1;
                    } else {
                        mGTgspin = ActEntry.EMPTY_FIELD;
                    }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                mGTgspin = ActEntry.GTg_1;
            }
        });
    }
    private void setupSpinnerGC() {
        ArrayAdapter groundconditSpinnerAdapter = ArrayAdapter.createFromResource(this,
                R.array.array_ground_condit, android.R.layout.simple_spinner_item);
        groundconditSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        mGCspinner.setAdapter(groundconditSpinnerAdapter);
        mGCspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mGCspin = (String) parent.getItemAtPosition(position);
                if (mGCspin.equals(getString(R.string.not_chosen))) {
                    mGCspin = ActEntry.EMPTY_FIELD;
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                mGCspin = ActEntry.EMPTY_FIELD;
            }
        });
    }
    private void setupSpinnerPM() {
        ArrayAdapter protecmaterSpinnerAdapter = ArrayAdapter.createFromResource(this,
                R.array.array_protec_mater, android.R.layout.simple_spinner_item);
        protecmaterSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        mPMspinner.setAdapter(protecmaterSpinnerAdapter);
        mPMspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mPMspin = (String) parent.getItemAtPosition(position);
                if (mPMspin.equals(getString(R.string.not_chosen))) {
                    mPMspin = ActEntry.EMPTY_FIELD;
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                mPMspin = ActEntry.EMPTY_FIELD;
            }
        });
    }
    private void setupSpinnerPA() {
        ArrayAdapter protecavailabSpinnerAdapter = ArrayAdapter.createFromResource(this,
                R.array.array_protec_availab, android.R.layout.simple_spinner_item);
        protecavailabSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        mPAspinner.setAdapter(protecavailabSpinnerAdapter);
        mPAspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mPAspin = (String) parent.getItemAtPosition(position);
                    if (mPAspin.equals(getString(R.string.not_chosen))) {
                        mPAspin = ActEntry.EMPTY_FIELD;
                        LLProtecConditBlock.setVisibility(View.GONE);
                    } else if (mPAspin.equals(getString(R.string.pa1))) {
                        mPAspin = ActEntry.PA_1;
                        LLProtecConditBlock.setVisibility(View.VISIBLE);
                    } else if (mPAspin.equals(getString(R.string.pa2))) {
                        mPAspin = ActEntry.PA_2;
                        LLProtecConditBlock.setVisibility(View.GONE);
                    } else {
                        mPAspin = ActEntry.EMPTY_FIELD;
                    }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                mPAspin = ActEntry.EMPTY_FIELD;
                LLProtecConditBlock.setVisibility(View.GONE);
            }
        });
    }
    private void setupSpinnerPC() {
        ArrayAdapter protecconditSpinnerAdapter = ArrayAdapter.createFromResource(this,
                R.array.array_protec_condit, android.R.layout.simple_spinner_item);
        protecconditSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        mPCspinner.setAdapter(protecconditSpinnerAdapter);
        mPCspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mPCspin = (String) parent.getItemAtPosition(position);
                if (mPCspin.equals(getString(R.string.not_chosen))) {
                    mPCspin = ActEntry.EMPTY_FIELD;
                } else if (mPCspin.equals(getString(R.string.pc1))) {
                    mPCspin = ActEntry.PC_1;
                } else if (mPCspin.equals(getString(R.string.pc2))) {
                    mPCspin = ActEntry.PC_2;
                } else {
                    mPCspin = ActEntry.EMPTY_FIELD;
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                mPCspin = ActEntry.EMPTY_FIELD;
            }
        });
    }
    private void setupSpinnerAD() {
        ArrayAdapter adgesionSpinnerAdapter = ArrayAdapter.createFromResource(this,
                R.array.array_adgesion, android.R.layout.simple_spinner_item);
        adgesionSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        mADspinner.setAdapter(adgesionSpinnerAdapter);
        mADspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mADspin = (String) parent.getItemAtPosition(position);
                if (mADspin.equals(getString(R.string.not_chosen))) {
                    mADspin = ActEntry.EMPTY_FIELD;
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                mADspin = ActEntry.EMPTY_FIELD;
            }
        });
    }
    private void setupSpinnerDTy() {
        ArrayAdapter damagetypeSpinnerAdapter = ArrayAdapter.createFromResource(this,
                R.array.array_damage_type, android.R.layout.simple_spinner_item);
        damagetypeSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        mDTyspinner.setAdapter(damagetypeSpinnerAdapter);
        mDTyspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mDTyspin = (String) parent.getItemAtPosition(position);
                if (mDTyspin.equals(getString(R.string.not_chosen))) {
                    mDTyspin = ActEntry.EMPTY_FIELD;
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                mDTyspin = ActEntry.EMPTY_FIELD;
            }
        });
    }
    private void setupSpinnerDA() {
        ArrayAdapter damageavailabSpinnerAdapter = ArrayAdapter.createFromResource(this,
                R.array.array_damage_availab, android.R.layout.simple_spinner_item);
        damageavailabSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        mDAspinner.setAdapter(damageavailabSpinnerAdapter);
        mDAspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mDAspin = (String) parent.getItemAtPosition(position);
                if (mDAspin.equals(getString(R.string.not_chosen))) {
                    mDAspin = ActEntry.EMPTY_FIELD;
                } else if (mDAspin.equals(getString(R.string.da1))) {
                    mDAspin = ActEntry.DA_1;
                    LL_DAsquareblock.setVisibility(View.VISIBLE);
                } else if (mDAspin.equals(getString(R.string.da2))) {
                    mDAspin = ActEntry.DA_2;
                    mDamageArea.setText("");
                    LL_DAsquareblock.setVisibility(View.GONE);
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                mDAspin = ActEntry.EMPTY_FIELD;
            }
        });
    }
    private void setupSpinnerCD() {
        ArrayAdapter corrdamageSpinnerAdapter = ArrayAdapter.createFromResource(this,
                R.array.array_corr_damage, android.R.layout.simple_spinner_item);
        corrdamageSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        mCDspinner.setAdapter(corrdamageSpinnerAdapter);
        mCDspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mCDspin = (String) parent.getItemAtPosition(position);
                if (mCDspin.equals(getString(R.string.not_chosen))) {
                    mCDspin = ActEntry.EMPTY_FIELD;
                    mCDCspin = ActEntry.EMPTY_FIELD;
                    LL_CDCblock.setVisibility(View.GONE);
                } else if (mCDspin.equals(getString(R.string.cd1))) {
                    mCDspin = ActEntry.CD_1;
                    LL_CDCblock.setVisibility(View.VISIBLE);
                } else if (mCDspin.equals(getString(R.string.cd2))) {
                    mCDspin = ActEntry.CD_2;
                    mCDCspin = ActEntry.EMPTY_FIELD;
                    LL_CDCblock.setVisibility(View.GONE);
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                mCDspin = ActEntry.EMPTY_FIELD;
            }
        });
    }
    private void setupSpinnerCDC() {
        ArrayAdapter corrdamagecharSpinnerAdapter = ArrayAdapter.createFromResource(this,
                R.array.array_corr_dam_char, android.R.layout.simple_spinner_item);
        corrdamagecharSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        mCDCspinner.setAdapter(corrdamagecharSpinnerAdapter);
        mCDCspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mCDCspin = (String) parent.getItemAtPosition(position);
                if (mCDCspin.equals(getString(R.string.not_chosen))) {
                    mCDCspin = ActEntry.EMPTY_FIELD;
                } else if(mCDCspin.equals(getString(R.string.cdc1))){
                    mCDCspin = ActEntry.CDC_1;
                } else if(mCDCspin.equals(getString(R.string.cdc2))){
                    mCDCspin = ActEntry.CDC_2;
                } else if(mCDCspin.equals(getString(R.string.cdc3))){
                    mCDCspin = ActEntry.CDC_3;
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                mCDCspin = ActEntry.EMPTY_FIELD;
            }
        });
    }
    private void setupSpinnerCDM() {
        ArrayAdapter corrdammaxSpinnerAdapter = ArrayAdapter.createFromResource(this,
                R.array.array_corr_dam_max, android.R.layout.simple_spinner_item);
        corrdammaxSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        mCDMspinner.setAdapter(corrdammaxSpinnerAdapter);
        mCDMspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mCDMspin = (String) parent.getItemAtPosition(position);
                if (mCDMspin.equals(getString(R.string.not_chosen))) {
                    mCDMspin = ActEntry.EMPTY_FIELD;
                } else if (mCDMspin.equals("1")) {
                    LL_CD1block.setVisibility(View.VISIBLE);
                    LL_CD13block.setVisibility(View.GONE);
                    LL_CD3block.setVisibility(View.GONE);
                } else if (mCDMspin.equals("2")) {
                    LL_CD1block.setVisibility(View.VISIBLE);
                    LL_CD13block.setVisibility(View.VISIBLE);
                    LL_CD3block.setVisibility(View.GONE);
                } else if (mCDMspin.equals("3") | mCDMspin.equals("4")| mCDMspin.equals("5")| mCDMspin.equals("6")| mCDMspin.equals("7")) {
                    LL_CD1block.setVisibility(View.VISIBLE);
                    LL_CD13block.setVisibility(View.VISIBLE);
                    LL_CD3block.setVisibility(View.VISIBLE);
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                mCDMspin = ActEntry.EMPTY_FIELD;
            }
        });
    }
    private void setupSpinnerCD1() {
        ArrayAdapter corrdam1SpinnerAdapter = ArrayAdapter.createFromResource(this,
                R.array.array_corr_damage_values, android.R.layout.simple_spinner_item);
        corrdam1SpinnerAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        mCD1spinner.setAdapter(corrdam1SpinnerAdapter);
        mCD1spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mCD1spin = (String) parent.getItemAtPosition(position);
                if (mCD1spin.equals(getString(R.string.not_chosen))) {
                    mCD1spin = ActEntry.EMPTY_FIELD;
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                mCD1spin = ActEntry.EMPTY_FIELD;
            }
        });
    }
    private void setupSpinnerCD13() {
        ArrayAdapter corrdam13SpinnerAdapter = ArrayAdapter.createFromResource(this,
                R.array.array_corr_damage_values, android.R.layout.simple_spinner_item);
        corrdam13SpinnerAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        mCD13spinner.setAdapter(corrdam13SpinnerAdapter);
        mCD13spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mCD13spin = (String) parent.getItemAtPosition(position);
                if (mCD13spin.equals(getString(R.string.not_chosen))) {
                    mCD13spin = ActEntry.EMPTY_FIELD;
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                mCD13spin = ActEntry.EMPTY_FIELD;
            }
        });
    }
    private void setupSpinnerCD3() {
        ArrayAdapter corrdam3SpinnerAdapter = ArrayAdapter.createFromResource(this,
                R.array.array_corr_damage_values, android.R.layout.simple_spinner_item);
        corrdam3SpinnerAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        mCD3spinner.setAdapter(corrdam3SpinnerAdapter);
        mCD3spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mCD3spin = (String) parent.getItemAtPosition(position);
                if (mCD3spin.equals(getString(R.string.not_chosen))) {
                    mCD3spin = ActEntry.EMPTY_FIELD;
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                mCD3spin = ActEntry.EMPTY_FIELD;
            }
        });
    }
    private void setupSpinnerOM() {
        ArrayAdapter othermaterSpinnerAdapter = ArrayAdapter.createFromResource(this,
                R.array.array_protec_mater, android.R.layout.simple_spinner_item);
        othermaterSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        mOMspinner.setAdapter(othermaterSpinnerAdapter);
        mOMspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mOMspin = (String) parent.getItemAtPosition(position);
                if (mOMspin.equals(getString(R.string.not_chosen))) {
                    mOMspin = ActEntry.EMPTY_FIELD;
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                mOMspin = ActEntry.EMPTY_FIELD;
            }
        });
    }

    private int mFilialSpinnerPosition;
    private int mOrganizationSpinnerAvailable = 0;

    private void setupSpinnerOrg1() {
        Log.v("SPINNERS LIVE", "ORIGINAL    SPINNER    IS      START");
        ArrayAdapter org1SpinnerAdapter = ArrayAdapter.createFromResource(this,
                R.array.array_org_first, android.R.layout.simple_spinner_item);
        org1SpinnerAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        mOrg1spinner.setAdapter(org1SpinnerAdapter);

         View.OnTouchListener spinnerOnTouch = new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    mOrganizationSpinnerAvailable = 1;
                }
                return false;
            }
        };
        mOrg1spinner.setOnTouchListener(spinnerOnTouch);

        mOrg1spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mOrg1spin = (String) parent.getItemAtPosition(position);
                mFilialSpinnerPosition = position;
                if (mOrganizationSpinnerAvailable == 1) {
                    organizationSpinnerAdapterSetting(position);
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                mOrg1spin = ActEntry.EMPTY_FIELD;
            }
        });
    }

    private void setupSpinnerOrg2() {
        Log.v("SPINNERS LIVE", "ORIGINAL FILIAL SPINNER IS START");
        organizationSpinnerAdapterSetting(mFilialSpinnerPosition);
        mOrg2spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mOrg2spin = (String) parent.getItemAtPosition(position);
                if (mOrg2spin.equals(getString(R.string.not_chosen))) {
                    mOrg2spin = ActEntry.EMPTY_FIELD;
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                mOrg2spin = ActEntry.EMPTY_FIELD;
            }
        });

    }

    /**
     * Метод для установки нужного адаптера в спиннер филиала организации. На входе получает позицию выбранной головной организации.
     */
    private void organizationSpinnerAdapterSetting(int position){
        if (position == 0) {
            mOrg1spin = ActEntry.EMPTY_FIELD;
        } else if (position == 1) {
            ArrayAdapter org2SpinnerAdapter = ArrayAdapter.createFromResource(getBaseContext(), R.array.array_org_filial_1, android.R.layout.simple_spinner_item);
            org2SpinnerAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
            mOrg2spinner.setAdapter(org2SpinnerAdapter);
        } else if (position == 2) {
            ArrayAdapter org2SpinnerAdapter = ArrayAdapter.createFromResource(getBaseContext(), R.array.array_org_filial_2, android.R.layout.simple_spinner_item);
            org2SpinnerAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
            mOrg2spinner.setAdapter(org2SpinnerAdapter);
        } else if (position == 3) {
            ArrayAdapter org2SpinnerAdapter = ArrayAdapter.createFromResource(getBaseContext(), R.array.array_org_filial_3, android.R.layout.simple_spinner_item);
            org2SpinnerAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
            mOrg2spinner.setAdapter(org2SpinnerAdapter);
        } else if (position == 4) {
            ArrayAdapter org2SpinnerAdapter = ArrayAdapter.createFromResource(getBaseContext(), R.array.array_org_filial_4, android.R.layout.simple_spinner_item);
            org2SpinnerAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
            mOrg2spinner.setAdapter(org2SpinnerAdapter);
        } else if (position == 5) {
            ArrayAdapter org2SpinnerAdapter = ArrayAdapter.createFromResource(getBaseContext(), R.array.array_org_filial_5, android.R.layout.simple_spinner_item);
            org2SpinnerAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
            mOrg2spinner.setAdapter(org2SpinnerAdapter);
        } else if (position == 6) {
            ArrayAdapter org2SpinnerAdapter = ArrayAdapter.createFromResource(getBaseContext(), R.array.array_org_filial_6, android.R.layout.simple_spinner_item);
            org2SpinnerAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
            mOrg2spinner.setAdapter(org2SpinnerAdapter);
        } else if (position == 7) {
            ArrayAdapter org2SpinnerAdapter = ArrayAdapter.createFromResource(getBaseContext(), R.array.array_org_filial_7, android.R.layout.simple_spinner_item);
            org2SpinnerAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
            mOrg2spinner.setAdapter(org2SpinnerAdapter);
        } else if (position == 8) {
            ArrayAdapter org2SpinnerAdapter = ArrayAdapter.createFromResource(getBaseContext(), R.array.array_org_filial_8, android.R.layout.simple_spinner_item);
            org2SpinnerAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
            mOrg2spinner.setAdapter(org2SpinnerAdapter);
        } else if (position == 9) {
            ArrayAdapter org2SpinnerAdapter = ArrayAdapter.createFromResource(getBaseContext(), R.array.array_org_filial_9, android.R.layout.simple_spinner_item);
            org2SpinnerAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
            mOrg2spinner.setAdapter(org2SpinnerAdapter);
        } else if (position == 10) {
            ArrayAdapter org2SpinnerAdapter = ArrayAdapter.createFromResource(getBaseContext(), R.array.array_org_filial_10, android.R.layout.simple_spinner_item);
            org2SpinnerAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
            mOrg2spinner.setAdapter(org2SpinnerAdapter);
        } else if (position == 11) {
            ArrayAdapter org2SpinnerAdapter = ArrayAdapter.createFromResource(getBaseContext(), R.array.array_org_filial_11, android.R.layout.simple_spinner_item);
            org2SpinnerAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
            mOrg2spinner.setAdapter(org2SpinnerAdapter);
        } else if (position == 12) {
            ArrayAdapter org2SpinnerAdapter = ArrayAdapter.createFromResource(getBaseContext(), R.array.array_org_filial_12, android.R.layout.simple_spinner_item);
            org2SpinnerAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
            mOrg2spinner.setAdapter(org2SpinnerAdapter);
        } else if (position == 13) {
            ArrayAdapter org2SpinnerAdapter = ArrayAdapter.createFromResource(getBaseContext(), R.array.array_org_filial_13, android.R.layout.simple_spinner_item);
            org2SpinnerAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
            mOrg2spinner.setAdapter(org2SpinnerAdapter);
        } else if (position == 14) {
            ArrayAdapter org2SpinnerAdapter = ArrayAdapter.createFromResource(getBaseContext(), R.array.array_org_filial_14, android.R.layout.simple_spinner_item);
            org2SpinnerAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
            mOrg2spinner.setAdapter(org2SpinnerAdapter);
        } else if (position == 15) {
            ArrayAdapter org2SpinnerAdapter = ArrayAdapter.createFromResource(getBaseContext(), R.array.array_org_filial_15, android.R.layout.simple_spinner_item);
            org2SpinnerAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
            mOrg2spinner.setAdapter(org2SpinnerAdapter);
        } else if (position == 16) {
            ArrayAdapter org2SpinnerAdapter = ArrayAdapter.createFromResource(getBaseContext(), R.array.array_org_filial_16, android.R.layout.simple_spinner_item);
            org2SpinnerAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
            mOrg2spinner.setAdapter(org2SpinnerAdapter);
        } else if (position == 17) {
            ArrayAdapter org2SpinnerAdapter = ArrayAdapter.createFromResource(getBaseContext(), R.array.array_org_filial_17, android.R.layout.simple_spinner_item);
            org2SpinnerAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
            mOrg2spinner.setAdapter(org2SpinnerAdapter);
        } else if (position == 18) {
            ArrayAdapter org2SpinnerAdapter = ArrayAdapter.createFromResource(getBaseContext(), R.array.array_org_filial_18, android.R.layout.simple_spinner_item);
            org2SpinnerAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
            mOrg2spinner.setAdapter(org2SpinnerAdapter);
        }

    }
    /**
     * Get user input from editor and save act into database.
     */

    private void saveAct() {
        // Read from input fields
        // Use trim to eliminate leading or trailing white space
        String number = mActNumb.getText().toString().trim();
         actdate = mDate.getText().toString().trim();
        String object = mObjectName.getText().toString().trim();
        String coord = mCoordPlace.getText().toString().trim();
        String thickness = mWallThickness.getText().toString().trim();
        String gps = tvLocationGPS.getText().toString().trim();
        String grounddepth = mGroundDepth.getText().toString().trim();
        String groundresist = mGroundResist.getText().toString().trim();
        String protecthickness = mProtecThickness.getText().toString().trim();
        String damagearea = mDamageArea.getText().toString().trim();
        String otherpoten = mOtherPoten.getText().toString().trim();
        String orguser = mOrgUser.getText().toString().trim();


        if (ImageView.getDrawable() != null) {
            Bitmap b = ((BitmapDrawable) ImageView.getDrawable()).getBitmap();
            Bitmap converetdImage = getResizedBitmap(b, 600);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            converetdImage.compress(Bitmap.CompressFormat.JPEG, 50, bos);
            byte[] img = bos.toByteArray();
            image64 = Base64.encodeToString(img, Base64.DEFAULT);
        }


        String photocomment = mPhotoComment.getText().toString().trim();


        // Добавить остальные внизу(!)---------------------------------------

        // Check if this is supposed to be a new act
        // and check if all the fields in the editor are blank
        if (mCurrentActUri == null &&
                TextUtils.isEmpty(number) && TextUtils.isEmpty(actdate) &&
                TextUtils.isEmpty(object)&& TextUtils.isEmpty(coord)&& TextUtils.isEmpty(thickness)&& TextUtils.isEmpty(gps)&& mLspin == ActEntry.EMPTY_FIELD&& mDspin == ActEntry.EMPTY_FIELD)
            {
            // Since no fields were modified, we can return early without creating a new act.
            // No need to create ContentValues and no need to do any ContentProvider operations.
            return;
        }

        // Create a ContentValues object where column names are the keys,
        // and act attributes from the editor are the values.
        ContentValues values = new ContentValues();

        values.put(ActEntry.COLUMN_1_DATE, actdate);
        values.put(ActEntry.COLUMN_1_OBJECT_NAME, object);
        values.put(ActEntry.COLUMN_1_COORD_PLACE, coord);
        values.put(ActEntry.COLUMN_1_NOM_WALL_THICKNESS, thickness);
        values.put(ActEntry.COLUMN_1_GPS, gps);
        values.put(ActEntry.COLUMN_1_LENGHT_PIT, mLspin);
        values.put(ActEntry.COLUMN_1_OUTSIDE_DIAM, mDspin);
        values.put(ActEntry.COLUMN_1_BASE_FOR_PIT, mRspin);
        values.put(ActEntry.COLUMN_1_DESC_TERR, mDTspin);
        values.put(ActEntry.COLUMN_GROUND_DEPTH, grounddepth);
        values.put(ActEntry.COLUMN_GROUND_TYPE, mGTspin);
        values.put(ActEntry.COLUMN_GROUND_GRUNT, mGTgspin);
        values.put(ActEntry.COLUMN_GROUND_RESIST, groundresist);
        values.put(ActEntry.COLUMN_GROUND_CONDIT, mGCspin);
        values.put(ActEntry.COLUMN_PROTEC_MATER, mPMspin);
        values.put(ActEntry.COLUMN_PROTEC_AVAILAB, mPAspin);
        values.put(ActEntry.COLUMN_PROTEC_CONDIT, mPCspin);
        values.put(ActEntry.COLUMN_PROTEC_THICKNESS, protecthickness);
        values.put(ActEntry.COLUMN_ADGESION, mADspin);
        values.put(ActEntry.COLUMN_DAMAGE_TYPE, mDTyspin);
        values.put(ActEntry.COLUMN_DAMAGE_AVAILAB, mDAspin);
        values.put(ActEntry.COLUMN_DAMAGE_AREA, damagearea);
        values.put(ActEntry.COLUMN_CORR_DAMAGE, mCDspin);
        values.put(ActEntry.COLUMN_CORR_DAM_CHAR, mCDCspin);
        values.put(ActEntry.COLUMN_CORR_DAM_MAX, mCDMspin);
        values.put(ActEntry.COLUMN_CORR_DAM_1, mCD1spin);
        values.put(ActEntry.COLUMN_CORR_DAM_1_3, mCD13spin);
        values.put(ActEntry.COLUMN_CORR_DAM_3, mCD3spin);
        values.put(ActEntry.COLUMN_OTHER_POTEN, otherpoten);
        values.put(ActEntry.COLUMN_OTHER_MATER, mOMspin);
        values.put(ActEntry.COLUMN_IMAGE, image64);
        values.put(ActEntry.COLUMN_PHOTOCOMMENT, photocomment);
        values.put(ActEntry.COLUMN_ORGANIZATION_1, mOrg1spin);
        values.put(ActEntry.COLUMN_ORGANIZATION_2, mOrg2spin);
        values.put(ActEntry.COLUMN_ORGANIZATION_USER, orguser);

        int numb = 0;
        if (!TextUtils.isEmpty(number)) {
            numb = Integer.parseInt(number);
        }
        values.put(ActEntry.COLUMN_1_NUBM, numb);


        // Determine if this is a new or existing act by checking if mCurrentACTUri is null or not
        if (mCurrentActUri == null) {
            // This is a NEW act, so insert a new act into the provider,
            // returning the content URI for the new act.
            Uri newUri = getContentResolver().insert(ActEntry.CONTENT_URI, values);
            // Show a toast message depending on whether or not the insertion was successful.
            if (newUri == null) {
                // If the new content URI is null, then there was an error with insertion.
                Toast.makeText(this, getString(R.string.editor_insert_act_failed),
                        Toast.LENGTH_SHORT).show();
            } else {
                // Otherwise, the insertion was successful and we can display a toast.
                Toast.makeText(this, getString(R.string.editor_insert_act_successful),
                        Toast.LENGTH_SHORT).show();
            }
        } else {
            // Otherwise this is an EXISTING act, so update the act with content URI: mCurrentActUri
            // and pass in the new ContentValues. Pass in null for the selection and selection args
            // because mCurrentActUri will already identify the correct row in the database that
            // we want to modify.
            int rowsAffected = getContentResolver().update(mCurrentActUri, values, null, null);
            // Show a toast message depending on whether or not the update was successful.
            if (rowsAffected == 0) {
                // If no rows were affected, then there was an error with the update.
                Toast.makeText(this, getString(R.string.editor_update_act_failed),
                        Toast.LENGTH_SHORT).show();
            } else {
                // Otherwise, the update was successful and we can display a toast.
                Toast.makeText(this, getString(R.string.editor_update_act_successful),
                        Toast.LENGTH_SHORT).show();
            }
        }
        ImageView.setImageBitmap(null);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu options from the res/menu/menu_create.xml file.
        // This adds menu items to the app bar.
        getMenuInflater().inflate(R.menu.menu_create, menu);
        return true;
    }

    /**
     * This method is called after invalidateOptionsMenu(), so that the
     * menu can be updated (some menu items can be hidden or made visible).
     */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        // If this is a new act, hide the "Delete" menu item.
        if (mCurrentActUri == null) {
            MenuItem menuItem = menu.findItem(R.id.action_delete);
            menuItem.setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // User clicked on a menu option in the app bar overflow menu
        switch (item.getItemId()) {
            // Respond to a click on the "Save" menu option
            case R.id.action_save:
                // Save act to database
                saveAct();
                // Exit activity
                finish();
                return true;
            // Respond to a click on the "Delete" menu option
            case R.id.action_delete:
                // Pop up confirmation dialog for deletion
                showDeleteConfirmationDialog();
                return true;
            // Respond to a click on the "Up" arrow button in the app bar
            case android.R.id.home:

                // **Navigate back to parent activity (MainActivity)
                //NavUtils.navigateUpFromSameTask(this);

                // If the act hasn't changed, continue with navigating up to parent activity
                // which is the {@link CatalogActivity}.
                if (!mActHasChanged) {
                    NavUtils.navigateUpFromSameTask(CreateActivity.this);
                    return true;
                }

                // Otherwise if there are unsaved changes, setup a dialog to warn the user.
                // Create a click listener to handle the user confirming that
                // changes should be discarded.
                DialogInterface.OnClickListener discardButtonClickListener =
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                // User clicked "Discard" button, navigate to parent activity.
                                NavUtils.navigateUpFromSameTask(CreateActivity.this);
                            }
                        };
                // Show a dialog that notifies the user they have unsaved changes
                showUnsavedChangesDialog(discardButtonClickListener);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * This method is called when the back button is pressed.
     */
    @Override
    public void onBackPressed() {
        // If the act hasn't changed, continue with handling back button press
        if (!mActHasChanged) {
            super.onBackPressed();
            return;
        }

        // Otherwise if there are unsaved changes, setup a dialog to warn the user.
        // Create a click listener to handle the user confirming that changes should be discarded.
        DialogInterface.OnClickListener discardButtonClickListener =
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        // User clicked "Discard" button, close the current activity.
                        finish();
                    }
                };

        // Show dialog that there are unsaved changes
        showUnsavedChangesDialog(discardButtonClickListener);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        // Since the editor shows all act attributes, define a projection that contains
        // all columns from the act table
        String[] projection = {
                ActEntry._ID,
                ActEntry.COLUMN_1_NUBM,
                ActEntry.COLUMN_1_DATE,
                ActEntry.COLUMN_1_OBJECT_NAME,
                ActEntry.COLUMN_1_COORD_PLACE,
                ActEntry.COLUMN_1_LENGHT_PIT,
                ActEntry.COLUMN_1_GPS,
                ActEntry.COLUMN_1_OUTSIDE_DIAM,
                ActEntry.COLUMN_1_NOM_WALL_THICKNESS,
                ActEntry.COLUMN_1_BASE_FOR_PIT,
                ActEntry.COLUMN_1_DESC_TERR,
                ActEntry.COLUMN_GROUND_DEPTH,
                ActEntry.COLUMN_GROUND_TYPE,
                ActEntry.COLUMN_GROUND_GRUNT,
                ActEntry.COLUMN_GROUND_RESIST,
                ActEntry.COLUMN_GROUND_CONDIT,
                ActEntry.COLUMN_PROTEC_MATER,
                ActEntry.COLUMN_PROTEC_AVAILAB,
                ActEntry.COLUMN_PROTEC_CONDIT,
                ActEntry.COLUMN_PROTEC_THICKNESS,
                ActEntry.COLUMN_ADGESION,
                ActEntry.COLUMN_DAMAGE_TYPE,
                ActEntry.COLUMN_DAMAGE_AVAILAB,
                ActEntry.COLUMN_DAMAGE_AREA,
                ActEntry.COLUMN_CORR_DAMAGE,
                ActEntry.COLUMN_CORR_DAM_CHAR,
                ActEntry.COLUMN_CORR_DAM_MAX,
                ActEntry.COLUMN_CORR_DAM_1,
                ActEntry.COLUMN_CORR_DAM_1_3,
                ActEntry.COLUMN_CORR_DAM_3,
                ActEntry.COLUMN_OTHER_POTEN,
                ActEntry.COLUMN_OTHER_MATER,
                ActEntry.COLUMN_IMAGE,
                ActEntry.COLUMN_PHOTOCOMMENT,
                ActEntry.COLUMN_ORGANIZATION_1,
                ActEntry.COLUMN_ORGANIZATION_2,
                ActEntry.COLUMN_ORGANIZATION_USER
        };


        // This loader will execute the ContentProvider's query method on a background thread
        return new CursorLoader(this,   // Parent activity context
                mCurrentActUri,         // Query the content URI for the current act
                projection,             // Columns to include in the resulting Cursor
                null,                   // No selection clause
                null,                   // No selection arguments
                null);                  // Default sort order
    }

    @Override
    @SuppressWarnings("unchecked")
    // ВАЖНО. Отключение предупреждения здесь применяется для отключения предупреждения в месте для установки номера позиции спиннера.
    // Предупредение вида unchecked call to 'getposition(T)' as a member of raw type 'android.widget.ArrayAdapter'
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        // Bail early if the cursor is null or there is less than 1 row in the cursor
        if (cursor == null || cursor.getCount() < 1) {
            return;
        }

        // Proceed with moving to the first row of the cursor and reading data from it
        // (This should be the only row in the cursor)
        if (cursor.moveToFirst()) {
            // Find the columns of act attributes that we're interested in
            int numbColumnIndex = cursor.getColumnIndex(ActEntry.COLUMN_1_NUBM);
            int dateColumnIndex = cursor.getColumnIndex(ActEntry.COLUMN_1_DATE);
            int objectColumnIndex = cursor.getColumnIndex(ActEntry.COLUMN_1_OBJECT_NAME);
            int coordColumnIndex = cursor.getColumnIndex(ActEntry.COLUMN_1_COORD_PLACE);
            int lenghtColumnIndex = cursor.getColumnIndex(ActEntry.COLUMN_1_LENGHT_PIT);
            int gpsColomnIndex = cursor.getColumnIndex(ActEntry.COLUMN_1_GPS);
            int diamColomnIndex = cursor.getColumnIndex(ActEntry.COLUMN_1_OUTSIDE_DIAM);
            int thicknessColomnIndex = cursor.getColumnIndex(ActEntry.COLUMN_1_NOM_WALL_THICKNESS);
            int reasonColomnIndex = cursor.getColumnIndex(ActEntry.COLUMN_1_BASE_FOR_PIT);
            int descterrColomnIndex = cursor.getColumnIndex(ActEntry.COLUMN_1_DESC_TERR);
            int gdColomnIndex = cursor.getColumnIndex(ActEntry.COLUMN_GROUND_DEPTH);
            int gtColomnIndex = cursor.getColumnIndex(ActEntry.COLUMN_GROUND_TYPE);
            int gtgColomnIndex = cursor.getColumnIndex(ActEntry.COLUMN_GROUND_GRUNT);
            int grColomnIndex = cursor.getColumnIndex(ActEntry.COLUMN_GROUND_RESIST);
            int gcColomnIndex = cursor.getColumnIndex(ActEntry.COLUMN_GROUND_CONDIT);
            int pmColomnIndex = cursor.getColumnIndex(ActEntry.COLUMN_PROTEC_MATER);
            int paColomnIndex = cursor.getColumnIndex(ActEntry.COLUMN_PROTEC_AVAILAB);
            int pcColomnIndex = cursor.getColumnIndex(ActEntry.COLUMN_PROTEC_CONDIT);
            int protecthickColomnIndex = cursor.getColumnIndex(ActEntry.COLUMN_PROTEC_THICKNESS);
            int adgesionColomnIndex = cursor.getColumnIndex(ActEntry.COLUMN_ADGESION);
            int damagetypeColomnIndex = cursor.getColumnIndex(ActEntry.COLUMN_DAMAGE_TYPE);
            int damageavailabColomnIndex = cursor.getColumnIndex(ActEntry.COLUMN_DAMAGE_AVAILAB);
            int damageareaColomnIndex = cursor.getColumnIndex(ActEntry.COLUMN_DAMAGE_AREA);
            int corrdamageColomnIndex = cursor.getColumnIndex(ActEntry.COLUMN_CORR_DAMAGE);
            int corrdamcharColomnIndex = cursor.getColumnIndex(ActEntry.COLUMN_CORR_DAM_CHAR);
            int corrdammaxColomnIndex = cursor.getColumnIndex(ActEntry.COLUMN_CORR_DAM_MAX);
            int corrdam1ColomnIndex = cursor.getColumnIndex(ActEntry.COLUMN_CORR_DAM_1);
            int corrdam13ColomnIndex = cursor.getColumnIndex(ActEntry.COLUMN_CORR_DAM_1_3);
            int corrdam3ColomnIndex = cursor.getColumnIndex(ActEntry.COLUMN_CORR_DAM_3);
            int otherpotenColomnIndex = cursor.getColumnIndex(ActEntry.COLUMN_OTHER_POTEN);
            int othermaterColomnIndex = cursor.getColumnIndex(ActEntry.COLUMN_OTHER_MATER);

            int image64ColomnIndex = cursor.getColumnIndex(ActEntry.COLUMN_IMAGE);
            int photocommentColomnIndex = cursor.getColumnIndex(ActEntry.COLUMN_PHOTOCOMMENT);

            int org1ColomnIndex = cursor.getColumnIndex(ActEntry.COLUMN_ORGANIZATION_1);
            int org2ColomnIndex = cursor.getColumnIndex(ActEntry.COLUMN_ORGANIZATION_2);
            int orguserColomnIndex = cursor.getColumnIndex(ActEntry.COLUMN_ORGANIZATION_USER);


            // Extract out the value from the Cursor for the given column index
            int numb = cursor.getInt(numbColumnIndex);
            actdate = cursor.getString(dateColumnIndex);
            String org1 = cursor.getString(org1ColomnIndex);
            String org2 = cursor.getString(org2ColomnIndex);

            String object = cursor.getString(objectColumnIndex);
            String coord = cursor.getString(coordColumnIndex);
            String lenght = cursor.getString(lenghtColumnIndex);
            String gps = cursor.getString(gpsColomnIndex);
            String diam= cursor.getString(diamColomnIndex);
            String thickness = cursor.getString(thicknessColomnIndex);
            String reason = cursor.getString(reasonColomnIndex);
            String descterr = cursor.getString(descterrColomnIndex);

            String gd = cursor.getString(gdColomnIndex);
            String gt = cursor.getString(gtColomnIndex);
            String gtg = cursor.getString(gtgColomnIndex);
            String gr = cursor.getString(grColomnIndex);
            String gc = cursor.getString(gcColomnIndex);

            String pm = cursor.getString(pmColomnIndex);
            String pa = cursor.getString(paColomnIndex);
            String pc = cursor.getString(pcColomnIndex);
            String protecthick = cursor.getString(protecthickColomnIndex);
            String adgesion = cursor.getString(adgesionColomnIndex);
            String damagetype = cursor.getString(damagetypeColomnIndex);
            String damageavailab = cursor.getString(damageavailabColomnIndex);
            String damagearea = cursor.getString(damageareaColomnIndex);

            String corrdamage = cursor.getString(corrdamageColomnIndex);
            String corrdamchar = cursor.getString(corrdamcharColomnIndex);
            String corrdammax = cursor.getString(corrdammaxColomnIndex);
            String corrdam1 = cursor.getString(corrdam1ColomnIndex);
            String corrdam13 = cursor.getString(corrdam13ColomnIndex);
            String corrdam3 = cursor.getString(corrdam3ColomnIndex);

            String otherpoten = cursor.getString(otherpotenColomnIndex);
            String othermater = cursor.getString(othermaterColomnIndex);

            String image64 = cursor.getString(image64ColomnIndex);
            String photocomment = cursor.getString(photocommentColomnIndex);
            String orguser = cursor.getString(orguserColomnIndex);

            // Update the views on the screen with the values from the database
            mActNumb.setText(Integer.toString(numb));
            mDate.setText(actdate);
            mObjectName.setText(object);
            mCoordPlace.setText(coord);
            tvLocationGPS2.setText(gps);
            mWallThickness.setText(thickness);
            mGroundDepth.setText(gd);
            mGroundResist.setText(gr);
            mProtecThickness.setText(protecthick);
            mDamageArea.setText(damagearea);
            mOtherPoten.setText(otherpoten);
            mOrgUser.setText(orguser);

            if (image64 != null) {
                byte[] decodedString = Base64.decode(image64, Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                ImageView.setImageBitmap(decodedByte);
            }

            mPhotoComment.setText(photocomment);


            // lspin is a dropdown spinner, so map the constant value from the database
            // into one of the dropdown options .
            // Then call setSelection() so that option is displayed on screen as the current selection.
            mOrg1spinner.setSelection(( (ArrayAdapter<String> ) mOrg1spinner.getAdapter()).getPosition(org1));
            Log.v("Here shold be org2", org2);
            mFilialSpinnerPosition = ( (ArrayAdapter<String> ) mOrg1spinner.getAdapter()).getPosition(org1);
            setupSpinnerOrg2();
            try {
                mOrg2spinner.setSelection(((ArrayAdapter<String>) mOrg2spinner.getAdapter()).getPosition(org2));
            } catch (Exception ex){
                Log.v("ERROR", "ALARM! ERROR! NULL HERE! FIX IT!");
            }


            mLspinner.setSelection(( (ArrayAdapter<String> ) mLspinner.getAdapter()).getPosition(lenght));
            mDspinner.setSelection(( (ArrayAdapter<String> ) mDspinner.getAdapter()).getPosition(diam));
            mRspinner.setSelection(( (ArrayAdapter<String> ) mRspinner.getAdapter()).getPosition(reason));
            mDTspinner.setSelection(( (ArrayAdapter<String> ) mDTspinner.getAdapter()).getPosition(descterr));
            mGTspinner.setSelection(( (ArrayAdapter<String> ) mGTspinner.getAdapter()).getPosition(gt));

            switch (gtg) {
                case ActEntry.EMPTY_FIELD:
                    mGTgspinner.setSelection(0);
                    break;
                case ActEntry.GTg_2:
                    mGTgspinner.setSelection(1);
                    break;
                case ActEntry.GTg_1:
                    mGTgspinner.setSelection(2);
                    break;
                default:
                    mGTgspinner.setSelection(0);
                    break;
            }
            mGCspinner.setSelection(( (ArrayAdapter<String> ) mGCspinner.getAdapter()).getPosition(gc));
            mPMspinner.setSelection(( (ArrayAdapter<String> ) mPMspinner.getAdapter()).getPosition(pm));

            switch (pa) {
                case ActEntry.EMPTY_FIELD:
                    mPAspinner.setSelection(0);
                    break;
                case ActEntry.PA_1:
                    mPAspinner.setSelection(1);
                    break;
                case ActEntry.PA_2:
                    mPAspinner.setSelection(2);
                    break;
                default:
                    mPAspinner.setSelection(0);
                    break;
            }
            switch (pc) {
                case ActEntry.EMPTY_FIELD:
                    mPCspinner.setSelection(0);
                    break;
                case ActEntry.PC_1:
                    mPCspinner.setSelection(1);
                    break;
                case ActEntry.PC_2:
                    mPCspinner.setSelection(2);
                    break;
                default:
                    mPCspinner.setSelection(0);
                    break;
            }

            mADspinner.setSelection(( (ArrayAdapter<String> ) mADspinner.getAdapter()).getPosition(adgesion));
            mDTyspinner.setSelection(( (ArrayAdapter<String> ) mDTyspinner.getAdapter()).getPosition(damagetype));
            mDAspinner.setSelection(( (ArrayAdapter<String> ) mDAspinner.getAdapter()).getPosition(damageavailab));


            switch (corrdamage) {
                case ActEntry.EMPTY_FIELD:
                    mCDspinner.setSelection(0);
                    break;
                case ActEntry.CD_1:
                    mCDspinner.setSelection(1);
                    break;
                case ActEntry.CD_2:
                    mCDspinner.setSelection(2);
                    break;
                default:
                    mCDspinner.setSelection(0);
                    break;
            }
            switch (corrdamchar) {
                case ActEntry.EMPTY_FIELD:
                    mCDCspinner.setSelection(0);
                    break;
                case ActEntry.CDC_1:
                    mCDCspinner.setSelection(1);
                    break;
                case ActEntry.CDC_2:
                    mCDCspinner.setSelection(2);
                    break;
                case ActEntry.CDC_3:
                    mCDCspinner.setSelection(3);
                    break;
                default:
                    mCDCspinner.setSelection(0);
                    break;
            }
            mCDMspinner.setSelection(( (ArrayAdapter<String> ) mCDMspinner.getAdapter()).getPosition(corrdammax));
            mCD1spinner.setSelection(( (ArrayAdapter<String> ) mCD1spinner.getAdapter()).getPosition(corrdam1));
            mCD13spinner.setSelection(( (ArrayAdapter<String> ) mCD13spinner.getAdapter()).getPosition(corrdam13));
            mCD3spinner.setSelection(( (ArrayAdapter<String> ) mCD3spinner.getAdapter()).getPosition(corrdam3));
            mOMspinner.setSelection(( (ArrayAdapter<String> ) mOMspinner.getAdapter()).getPosition(othermater));

        }
    }


    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        // If the loader is invalidated, clear out all the data from the input fields.
        mActNumb.setText("");
        mDate.setText("");
        mObjectName.setText("");
        mCoordPlace.setText("");
        mWallThickness.setText("");
        mLspinner.setSelection(0);
        mDspinner.setSelection(0);
        mGroundDepth.setText("");
        mGroundResist.setText("");
        mGTspinner.setSelection(0);
        mGCspinner.setSelection(0);
        mPMspinner.setSelection(0);
        mPAspinner.setSelection(0);
        mPCspinner.setSelection(0);
        mProtecThickness.setText("");
        mADspinner.setSelection(0);
        mDTyspinner.setSelection(0);
        mDAspinner.setSelection(0);
        mDamageArea.setText("");
        mCDspinner.setSelection(0);
        mCDCspinner.setSelection(0);
        mCDMspinner.setSelection(0);
        mCD1spinner.setSelection(0);
        mCD13spinner.setSelection(0);
        mCD3spinner.setSelection(0);
        mOtherPoten.setText("");
        mOMspinner.setSelection(0);

    }
    /**
     * Show a dialog that warns the user there are unsaved changes that will be lost
     * if they continue leaving the editor.
     *
     * @param discardButtonClickListener is the click listener for what to do when
     *                                   the user confirms they want to discard their changes
     */
    private void showUnsavedChangesDialog(
            DialogInterface.OnClickListener discardButtonClickListener) {
        // Create an AlertDialog.Builder and set the message, and click listeners
        // for the postivie and negative buttons on the dialog.
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.unsaved_changes_dialog_msg);
        builder.setPositiveButton(R.string.discard, discardButtonClickListener);
        builder.setNegativeButton(R.string.keep_editing, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked the "Keep editing" button, so dismiss the dialog
                // and continue editing the act.
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });

        // Create and show the AlertDialog
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    /**
     * Prompt the user to confirm that they want to delete this act.
     */
    private void showDeleteConfirmationDialog() {
        // Create an AlertDialog.Builder and set the message, and click listeners
        // for the postivie and negative buttons on the dialog.
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.delete_dialog_msg);
        builder.setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked the "Delete" button, so delete the act.
                deleteAct();
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked the "Cancel" button, so dismiss the dialog
                // and continue editing the act.
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });

        // Create and show the AlertDialog
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    /**
     * Perform the deletion of the act in the database.
     */
    private void deleteAct() {
        // Only perform the delete if this is an existing act.
        if (mCurrentActUri != null) {
            // Call the ContentResolver to delete the act at the given content URI.
            // Pass in null for the selection and selection args because the mCurrentActUri
            // content URI already identifies the act that we want.
            int rowsDeleted = getContentResolver().delete(mCurrentActUri, null, null);

            // Show a toast message depending on whether or not the delete was successful.
            if (rowsDeleted == 0) {
                // If no rows were deleted, then there was an error with the delete.
                Toast.makeText(this, getString(R.string.editor_delete_act_failed),
                        Toast.LENGTH_SHORT).show();
            } else {
                // Otherwise, the delete was successful and we can display a toast.
                Toast.makeText(this, getString(R.string.editor_delete_act_successful),
                        Toast.LENGTH_SHORT).show();
            }
        }
        // Close the activity
        finish();
    }


    @Override
    protected void onPause() {
        super.onPause();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.removeUpdates(locationListener);
    }
    private LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            showLocation(location);
        }
        @Override
        public void onProviderDisabled(String provider) {
            checkEnabled1();
        }
        @Override
        public void onProviderEnabled(String provider) {
            checkEnabled1();
        }
        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            if (provider.equals(LocationManager.GPS_PROVIDER)) {
                tvStatusGPS.setText(getString(R.string.Status)+" " + String.valueOf(status));
            }
        }
    };

    private void showLocation(Location location) {
        if (location == null)
            return;
        if (location.getProvider().equals(LocationManager.GPS_PROVIDER)) {
            tvLocationGPS.setText(formatLocation(location));
        }
    }
    private String formatLocation(Location location) {
        if (location == null)
            return "";
        return String.format(
                "Координаты: Широта = %1$.4f, Долгота = %2$.4f",
                location.getLatitude(), location.getLongitude());
    }

    private void checkEnabled1() {
        tvEnabledGPS.setText(getString(R.string.Enabled)+" "
                + locationManager
                .isProviderEnabled(LocationManager.GPS_PROVIDER));
    }

    public void onClickLocationSettings(View view) {
        startActivity(new Intent(
                android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
    }

    public void onClickWeb(View v) {
//        String number = mActNumb.getText().toString().trim();
//        String object = mObjectName.getText().toString().trim();
//        String coord = mCoordPlace.getText().toString().trim();
//        String thickness = mWallThickness.getText().toString().trim();
//        String gps = tvLocationGPS.getText().toString().trim();
//        String grounddepth = mGroundDepth.getText().toString().trim();
//        String groundresist = mGroundResist.getText().toString().trim();
//        String protecthick = mProtecThickness.getText().toString().trim();
//        String damagearea = mDamageArea.getText().toString().trim();
//        String otherpoten = mOtherPoten.getText().toString().trim();
//        String orguser = mOrgUser.getText().toString().trim();
//        int days = DateDialog.dayS;
//        int month = DateDialog.monthS;
//        int year = DateDialog.yearS;
//
//        if (ImageView.getDrawable() != null) {
//            Bitmap b = ((BitmapDrawable) ImageView.getDrawable()).getBitmap();
//            Bitmap converetdImage = getResizedBitmap(b, 600);
//            ByteArrayOutputStream bos = new ByteArrayOutputStream();
//            converetdImage.compress(Bitmap.CompressFormat.JPEG, 50, bos);
//            byte[] img = bos.toByteArray();
//            image64 = Base64.encodeToString(img, Base64.DEFAULT);
//        }
//
//        String photocomment = mPhotoComment.getText().toString().trim();


        ActForm newAct = new ActForm(this);
        String actHtml = newAct.testmethod();
        Intent intent = new Intent(CreateActivity.this, WebActivity.class);
        intent.putExtra("htmltext", actHtml);

//                "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\"> <HTML> <HEAD> \t<META HTTP-EQUIV=\"CONTENT-TYPE\" CONTENT=\"text/html; charset=windows-1252\"> \t<TITLE></TITLE> \t<META NAME=\"GENERATOR\" CONTENT=\"OpenOffice 4.1.2  (Win32)\"> \t<META NAME=\"CREATED\" CONTENT=\"20090416;11320264\"> \t<META NAME=\"CHANGED\" CONTENT=\"20160831;19205912\"> \t<META NAME=\"Info 1\" CONTENT=\"\"> \t<META NAME=\"Info 2\" CONTENT=\"\"> \t<META NAME=\"Info 3\" CONTENT=\"\"> \t<META NAME=\"Info 4\" CONTENT=\"\"> \t<STYLE TYPE=\"text/css\"> \t<!-- \t\t@page { margin: 0.79in }         P { margin-bottom: 0.08in; so-language: zxx; line-height: 1.15; } \t\tA:link { so-language: zxx } \t--> \t</STYLE> </HEAD> <BODY LANG=\"de-DE\" DIR=\"LTR\"> <P LANG=\"ru-RU\" ALIGN=CENTER STYLE=\"margin-left: 5.91in; text-indent: -2.26in; margin-bottom: 0in; page-break-before: auto; page-break-after: auto\"> <B>&laquo;&#1059;&#1090;&#1074;&#1077;&#1088;&#1078;&#1076;&#1072;&#1102;&raquo;</B></P> <P LANG=\"ru-RU\" ALIGN=RIGHT STYLE=\"margin-right: 0.1in; margin-bottom: 0in; page-break-before: auto\"> ___________________________________</P> <P LANG=\"ru-RU\" ALIGN=RIGHT STYLE=\"margin-right: 0.1in; margin-bottom: 0in; page-break-before: auto; page-break-after: auto\"> (&#1076;&#1086;&#1083;&#1078;&#1085;&#1086;&#1089;&#1090;&#1100;, &#1085;&#1072;&#1080;&#1084;&#1077;&#1085;&#1086;&#1074;&#1072;&#1085;&#1080;&#1077; &#1047;&#1072;&#1082;&#1072;&#1079;&#1095;&#1080;&#1082;&#1072;)</P> <P LANG=\"ru-RU\" ALIGN=RIGHT STYLE=\"margin-right: 0.1in; margin-bottom: 0in\"> ____________    _____________________</P> <P LANG=\"ru-RU\" ALIGN=CENTER STYLE=\"margin-left: 3in; margin-right: 0.1in; margin-bottom: 0in; page-break-before: auto\"> (&#1087;&#1086;&#1076;&#1087;&#1080;&#1089;&#1100;) &emsp;&emsp;&emsp;&emsp;&emsp;(&#1060;.&#1048;.&#1054;)</P> <P LANG=\"ru-RU\" ALIGN=CENTER STYLE=\"margin-left: 3.3in; margin-right: 0.1in; margin-bottom: 0in\"> <SPAN LANG=\"en-US\">&laquo;____&raquo;</SPAN> <SPAN LANG=\"en-US\">________________</SPAN> <SPAN LANG=\"en-US\">20______</SPAN> &#1075;.</P> <P LANG=\"zxx\" ALIGN=RIGHT STYLE=\"margin-right: 0.1in; margin-bottom: 0in\"> <BR> </P> <P LANG=\"ru-RU\" ALIGN=CENTER STYLE=\"margin-bottom: 0in\"><B>&#1040;&#1082;&#1090; &#1096;&#1091;&#1088;&#1092;&#1086;&#1074;&#1082;&#1080; N <SPAN LANG=\"en-US\"><B>" + number +"</B></SPAN> &#1086;&#1090; <SPAN LANG=\"en-US\">"+"«"+days+"»"+month+" "+year+ "</SPAN> &#1075;.</B></P> <P LANG=\"zxx\" ALIGN=CENTER STYLE=\"margin-bottom: 0in\"><BR> </P> <OL> \t<LI><P LANG=\"ru-RU\" ALIGN=LEFT STYLE=\"margin-bottom: 0in; font-weight: normal\"> \t&#1053;&#1072;&#1080;&#1084;&#1077;&#1085;&#1086;&#1074;&#1072;&#1085;&#1080;&#1077; \t&#1086;&#1073;&#1098;&#1077;&#1082;&#1090;&#1072;: <SPAN LANG=\"en-US\"><B>"+object+"</B></SPAN></P> \t<P LANG=\"ru-RU\" ALIGN=LEFT STYLE=\"margin-bottom: 0in; font-weight: normal\"> \t&#1050;&#1086;&#1086;&#1088;&#1076;&#1080;&#1085;&#1072;&#1090;&#1072; \t&#1084;&#1077;&#1089;&#1090;&#1072; &#1096;&#1091;&#1088;&#1092;&#1086;&#1074;&#1072;&#1085;&#1080;&#1103;: \t<SPAN LANG=\"en-US\"><B>"+coord+"</B></SPAN>, &#1076;&#1083;&#1080;&#1085;&#1072; \t&#1096;&#1091;&#1088;&#1092;&#1072;: <SPAN LANG=\"en-US\"><B>"+mLspin+"</B></SPAN> \t</P> </OL> <P LANG=\"en-US\" ALIGN=LEFT STYLE=\"text-indent: 0.40in; margin-bottom: 0in; font-weight: normal; page-break-before: auto; page-break-after: auto\"> GPS(WGS 84): <B>"+gps+"</B></P> <OL START=2> \t<LI><P LANG=\"en-US\" ALIGN=LEFT STYLE=\"margin-bottom: 0in; font-weight: normal\"> \t<SPAN LANG=\"ru-RU\">&#1053;&#1072;&#1088;&#1091;&#1078;&#1085;&#1099;&#1081; \t&#1076;&#1080;&#1072;&#1084;&#1077;&#1090;&#1088; &#1090;&#1088;&#1091;&#1073;&#1086;&#1087;&#1088;&#1086;&#1074;&#1086;&#1076;&#1072;: \t</SPAN><B>"+mDspin+"</B><SPAN LANG=\"ru-RU\">, &#1090;&#1086;&#1083;&#1097;&#1080;&#1085;&#1072; \t&#1089;&#1090;&#1077;&#1085;&#1082;&#1080; &#1090;&#1088;&#1091;&#1073;&#1099;: \t</SPAN><B>"+thickness+"</B><SPAN LANG=\"ru-RU\"></SPAN></P> \t<LI><P LANG=\"en-US\" ALIGN=LEFT STYLE=\"margin-bottom: 0in; font-weight: normal\"> \t<SPAN LANG=\"ru-RU\">&#1054;&#1089;&#1085;&#1086;&#1074;&#1072;&#1085;&#1080;&#1077; \t&#1076;&#1083;&#1103; &#1087;&#1088;&#1086;&#1074;&#1077;&#1076;&#1077;&#1085;&#1080;&#1103; \t&#1096;&#1091;&#1088;&#1092;&#1086;&#1074;&#1072;&#1085;&#1080;&#1103;: \t</SPAN><B>"+mRspin+"</B></P> \t<LI><P LANG=\"ru-RU\" ALIGN=LEFT STYLE=\"margin-bottom: 0in; font-weight: normal\"> \t&#1052;&#1077;&#1089;&#1090;&#1085;&#1086;&#1089;&#1090;&#1100;: \t<SPAN LANG=\"en-US\"><B>"+mDTspin+"</B></SPAN></P> \t<LI><P LANG=\"ru-RU\" ALIGN=LEFT STYLE=\"margin-bottom: 0in; font-weight: normal\"> \t&#1043;&#1083;&#1091;&#1073;&#1080;&#1085;&#1072; &#1079;&#1072;&#1083;&#1086;&#1078;&#1077;&#1085;&#1080;&#1103; \t&#1086;&#1090; &#1074;&#1077;&#1088;&#1093;&#1085;&#1077;&#1081; \t&#1086;&#1073;&#1088;&#1072;&#1079;&#1091;&#1102;&#1097;&#1077;&#1081; \t&#1090;&#1088;&#1091;&#1073;&#1086;&#1087;&#1088;&#1086;&#1074;&#1086;&#1076;&#1072; \t&#1076;&#1086; &#1087;&#1086;&#1074;&#1077;&#1088;&#1093;&#1085;&#1086;&#1089;&#1090;&#1080; \t&#1079;&#1077;&#1084;&#1083;&#1080;: <SPAN LANG=\"en-US\"><B>"+grounddepth+"</B></SPAN> \t</P> \t<LI><P LANG=\"ru-RU\" ALIGN=LEFT STYLE=\"margin-bottom: 0in; font-weight: normal\"> \t&#1058;&#1080;&#1087; &#1075;&#1088;&#1091;&#1085;&#1090;&#1072;:     <SPAN LANG=\"en-US\"><B>"+mGTspin+mGTgspin+"</B></SPAN></P> \t<LI><P LANG=\"ru-RU\" ALIGN=LEFT STYLE=\"margin-bottom: 0in; font-weight: normal\"> \t&#1059;&#1076;&#1077;&#1083;&#1100;&#1085;&#1086;&#1077; \t&#1089;&#1086;&#1087;&#1088;&#1086;&#1090;&#1080;&#1074;&#1083;&#1077;&#1085;&#1080;&#1077; \t&#1075;&#1088;&#1091;&#1085;&#1090;&#1072;: <SPAN LANG=\"en-US\"><B>"+groundresist+"</B></SPAN> \t<SPAN LANG=\"en-US\"></SPAN></P> \t<LI><P LANG=\"ru-RU\" ALIGN=LEFT STYLE=\"margin-bottom: 0in; font-weight: normal\"> \t&#1057;&#1086;&#1089;&#1090;&#1086;&#1103;&#1085;&#1080;&#1077; \t&#1075;&#1088;&#1091;&#1085;&#1090;&#1072;: <SPAN LANG=\"en-US\"><B>"+mGCspin+"</B></SPAN></P> \t<LI><P LANG=\"ru-RU\" ALIGN=LEFT STYLE=\"margin-bottom: 0in; font-weight: normal\"> \t&#1052;&#1072;&#1090;&#1077;&#1088;&#1080;&#1072;&#1083; &#1079;&#1072;&#1097;&#1080;&#1090;&#1085;&#1086;&#1075;&#1086; \t&#1087;&#1086;&#1082;&#1088;&#1099;&#1090;&#1080;&#1103;: \t<SPAN LANG=\"en-US\"><B>"+mPMspin+"</B></SPAN></P> \t<LI><P LANG=\"ru-RU\" ALIGN=LEFT STYLE=\"margin-bottom: 0in; font-weight: normal\"> \t&#1058;&#1086;&#1083;&#1097;&#1080;&#1085;&#1072; &#1079;&#1072;&#1097;&#1080;&#1090;&#1085;&#1086;&#1075;&#1086; \t&#1087;&#1086;&#1082;&#1088;&#1099;&#1090;&#1080;&#1103;: \t<SPAN LANG=\"en-US\"><B>"+protecthick+"</B></SPAN></P> \t<LI><P LANG=\"ru-RU\" ALIGN=LEFT STYLE=\"margin-bottom: 0in; font-weight: normal\"> \t&#1040;&#1076;&#1075;&#1077;&#1079;&#1080;&#1103; &#1079;&#1072;&#1097;&#1080;&#1090;&#1085;&#1086;&#1075;&#1086; \t&#1087;&#1086;&#1082;&#1088;&#1099;&#1090;&#1080;&#1103;: <SPAN LANG=\"en-US\"><B>"+mADspin+"</B></SPAN></P> \t<LI><P LANG=\"ru-RU\" ALIGN=LEFT STYLE=\"margin-bottom: 0in; font-weight: normal\"> \t&#1061;&#1072;&#1088;&#1072;&#1082;&#1090;&#1077;&#1088; &#1087;&#1086;&#1074;&#1088;&#1077;&#1078;&#1076;&#1077;&#1085;&#1080;&#1081; \t&#1079;&#1072;&#1097;&#1080;&#1090;&#1085;&#1086;&#1075;&#1086; \t&#1087;&#1086;&#1082;&#1088;&#1099;&#1090;&#1080;&#1103;: \t<SPAN LANG=\"en-US\"><B>"+mDTyspin+"</B></SPAN></P> \t<LI><P LANG=\"ru-RU\" ALIGN=LEFT STYLE=\"margin-bottom: 0in; font-weight: normal\"> \t&#1053;&#1072;&#1083;&#1080;&#1095;&#1080;&#1077; &#1089;&#1082;&#1074;&#1086;&#1079;&#1085;&#1099;&#1093; \t&#1087;&#1086;&#1074;&#1088;&#1077;&#1078;&#1076;&#1077;&#1085;&#1080;&#1081; \t&#1079;&#1072;&#1097;&#1080;&#1090;&#1085;&#1086;&#1075;&#1086; \t&#1087;&#1086;&#1082;&#1088;&#1099;&#1090;&#1080;&#1103;: \t<SPAN LANG=\"en-US\"><B>"+mDAspin+"</B></SPAN></P> \t<LI><P LANG=\"ru-RU\" ALIGN=LEFT STYLE=\"margin-bottom: 0in; font-weight: normal\"> \t&#1054;&#1073;&#1097;&#1072;&#1103; &#1087;&#1083;&#1086;&#1097;&#1072;&#1076;&#1100; \t&#1087;&#1086;&#1074;&#1088;&#1077;&#1078;&#1076;&#1077;&#1085;&#1080;&#1081; \t&#1079;&#1072;&#1097;&#1080;&#1090;&#1085;&#1086;&#1075;&#1086; \t&#1087;&#1086;&#1082;&#1088;&#1099;&#1090;&#1080;&#1103;: \t<SPAN LANG=\"en-US\"><B>"+damagearea+"</B></SPAN></P> \t<LI><P LANG=\"ru-RU\" ALIGN=LEFT STYLE=\"margin-bottom: 0in; font-weight: normal\"> \t&#1054;&#1073;&#1077;&#1088;&#1090;&#1082;&#1072; &#1080; &#1077;&#1077; \t&#1089;&#1086;&#1089;&#1090;&#1086;&#1103;&#1085;&#1080;&#1077;: \t<SPAN LANG=\"en-US\"><B>"+mPAspin+mPCspin+"</B></SPAN></P> \t<LI><P LANG=\"ru-RU\" ALIGN=LEFT STYLE=\"margin-bottom: 0in; font-weight: normal\"> \t&#1053;&#1072;&#1083;&#1080;&#1095;&#1080;&#1077; &#1080; &#1093;&#1072;&#1088;&#1072;&#1082;&#1090;&#1077;&#1088; \t&#1082;&#1086;&#1088;&#1088;&#1086;&#1079;&#1080;&#1086;&#1085;&#1085;&#1099;&#1093; \t&#1087;&#1086;&#1074;&#1088;&#1077;&#1078;&#1076;&#1077;&#1085;&#1080;&#1081;: \t<SPAN LANG=\"en-US\"><B>"+mCDspin+mCDCspin+"</B></SPAN></P> \t<LI><P LANG=\"ru-RU\" ALIGN=LEFT STYLE=\"margin-bottom: 0in; font-weight: normal\"> \t&#1052;&#1072;&#1082;&#1089;&#1080;&#1084;&#1072;&#1083;&#1100;&#1085;&#1072;&#1103; \t&#1075;&#1083;&#1091;&#1073;&#1080;&#1085;&#1072; &#1082;&#1086;&#1088;&#1088;&#1086;&#1079;&#1080;&#1086;&#1085;&#1085;&#1099;&#1093; \t&#1087;&#1086;&#1074;&#1088;&#1077;&#1078;&#1076;&#1077;&#1085;&#1080;&#1081;: \t<SPAN LANG=\"en-US\"><B>"+mCDMspin+"</B></SPAN></P> \t<P LANG=\"ru-RU\" ALIGN=LEFT STYLE=\"margin-bottom: 0in; font-weight: normal\"> \t<SPAN LANG=\"en-US\">- </SPAN>&#1082;&#1086;&#1088;&#1088;&#1086;&#1079;&#1080;&#1086;&#1085;&#1085;&#1099;&#1093; \t&#1087;&#1086;&#1074;&#1088;&#1077;&#1078;&#1076;&#1077;&#1085;&#1080;&#1081; \t&#1089; &#1075;&#1083;&#1091;&#1073;&#1080;&#1085;&#1086;&#1081; &#1076;&#1086; \t1 &#1084;&#1084;: <SPAN LANG=\"en-US\"><B>"+mCD1spin+"</B></SPAN></P> \t<P LANG=\"ru-RU\" ALIGN=LEFT STYLE=\"margin-bottom: 0in; font-weight: normal\"> \t<SPAN LANG=\"en-US\">- </SPAN>&#1082;&#1086;&#1088;&#1088;&#1086;&#1079;&#1080;&#1086;&#1085;&#1085;&#1099;&#1093; \t&#1087;&#1086;&#1074;&#1088;&#1077;&#1078;&#1076;&#1077;&#1085;&#1080;&#1081; \t&#1089; &#1075;&#1083;&#1091;&#1073;&#1080;&#1085;&#1086;&#1081; &#1086;&#1090; \t1 &#1076;&#1086; 3 &#1084;&#1084;: <B>"+mCD13spin+"</B></P> \t<P LANG=\"ru-RU\" ALIGN=LEFT STYLE=\"margin-bottom: 0in; font-weight: normal\"> \t- &#1082;&#1086;&#1088;&#1088;&#1086;&#1079;&#1080;&#1086;&#1085;&#1085;&#1099;&#1093; \t&#1087;&#1086;&#1074;&#1088;&#1077;&#1078;&#1076;&#1077;&#1085;&#1080;&#1081; \t&#1089; &#1075;&#1083;&#1091;&#1073;&#1080;&#1085;&#1086;&#1081; \t&#1089;&#1074;&#1099;&#1096;&#1077; 3 &#1084;&#1084;: <SPAN LANG=\"en-US\"><B>"+mCD3spin+"</B></SPAN></P> \t<LI><P LANG=\"ru-RU\" ALIGN=LEFT STYLE=\"margin-bottom: 0in; font-weight: normal\"> \t&#1055;&#1086;&#1090;&#1077;&#1085;&#1094;&#1080;&#1072;&#1083; \t&laquo;&#1090;&#1088;&#1091;&#1073;&#1072;-&#1079;&#1077;&#1084;&#1083;&#1103;&raquo; \t&#1074; &#1096;&#1091;&#1088;&#1092;&#1077;: <SPAN LANG=\"en-US\"><B>"+otherpoten+"</B></SPAN></P> \t<LI><P LANG=\"ru-RU\" ALIGN=LEFT STYLE=\"margin-bottom: 0in; font-weight: normal\"> \t&#1052;&#1072;&#1090;&#1077;&#1088;&#1080;&#1072;&#1083; &#1079;&#1072;&#1097;&#1080;&#1090;&#1085;&#1086;&#1075;&#1086; \t&#1087;&#1086;&#1082;&#1088;&#1099;&#1090;&#1080;&#1103; &#1087;&#1088;&#1080; \t&#1088;&#1077;&#1084;&#1086;&#1085;&#1090;&#1077;: <SPAN LANG=\"en-US\"><B>"+mOMspin+"</B></SPAN></P> </OL> <P LANG=\"zxx\" ALIGN=LEFT STYLE=\"margin-bottom: 0in\"><BR> </P> <P LANG=\"ru-RU\" ALIGN=LEFT STYLE=\"margin-bottom: 0in; font-weight: normal\"> &#1050;&#1086;&#1085;&#1090;&#1088;&#1086;&#1083;&#1100; &#1089;&#1086;&#1089;&#1090;&#1086;&#1103;&#1085;&#1080;&#1103; &#1086;&#1073;&#1086;&#1088;&#1091;&#1076;&#1086;&#1074;&#1072;&#1085;&#1080;&#1103; &#1074;&#1099;&#1087;&#1086;&#1083;&#1085;&#1077;&#1085; &#1089; &#1087;&#1088;&#1080;&#1084;&#1077;&#1085;&#1077;&#1085;&#1080;&#1077;&#1084; &#1087;&#1088;&#1080;&#1073;&#1086;&#1088;&#1085;&#1086;&#1075;&#1086; &#1086;&#1073;&#1086;&#1088;&#1091;&#1076;&#1086;&#1074;&#1072;&#1085;&#1080;&#1103;: </P> <P LANG=\"en-US\" ALIGN=LEFT STYLE=\"margin-bottom: 0in; font-weight: normal\"> <B>#TOOLS#</B></P> <P LANG=\"ru-RU\" ALIGN=LEFT STYLE=\"margin-bottom: 0in; font-weight: normal\"> &#1055;&#1054;&#1044;&#1055;&#1048;&#1057;&#1040;&#1051;&#1048;:  </P> <P LANG=\"zxx\" ALIGN=LEFT STYLE=\"margin-bottom: 0in\"><SPAN LANG=\"ru-RU\"><SPAN STYLE=\"font-weight: normal\">&#1054;&#1090; &#1086;&#1073;&#1089;&#1083;&#1077;&#1076;&#1091;&#1102;&#1097;&#1077;&#1081; &#1086;&#1088;&#1075;&#1072;&#1085;&#1080;&#1079;&#1072;&#1094;&#1080;&#1080;</SPAN></SPAN><SPAN LANG=\"ru-RU\"><SPAN STYLE=\"font-weight: normal\"> </SPAN></SPAN><SPAN LANG=\"en-US\"><SPAN STYLE=\"font-weight: normal\"><B>"+orguser+"</B></SPAN></SPAN></P> <P LANG=\"en-US\" ALIGN=LEFT STYLE=\"margin-bottom: 0in; font-weight: normal\"> ________________ ______________________________ __________ ________________</P> <P LANG=\"ru-RU\" ALIGN=LEFT STYLE=\"text-indent: 0.21in; margin-bottom: 0in; font-weight: normal; page-break-before: auto; page-break-after: auto\"> (&#1076;&#1086;&#1083;&#1078;&#1085;&#1086;&#1089;&#1090;&#1100;)&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;(&#1060;.&#1048;.&#1054;.)&emsp;&emsp;&emsp;&emsp;&emsp;(&#1087;&#1086;&#1076;&#1087;&#1080;&#1089;&#1100;)&emsp;&emsp;&emsp;(&#1076;&#1072;&#1090;&#1072;)</P> <P LANG=\"en-US\" ALIGN=LEFT STYLE=\"margin-bottom: 0in; font-weight: normal; page-break-before: auto; page-break-after: auto\"> ________________ ______________________________ __________ ________________</P> <P LANG=\"ru-RU\" ALIGN=LEFT STYLE=\"text-indent: 0.21in; margin-bottom: 0in; font-weight: normal\"> (&#1076;&#1086;&#1083;&#1078;&#1085;&#1086;&#1089;&#1090;&#1100;)&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;(&#1060;.&#1048;.&#1054;.)&emsp;&emsp;&emsp;&emsp;&emsp;(&#1087;&#1086;&#1076;&#1087;&#1080;&#1089;&#1100;)&emsp;&emsp;&emsp;(&#1076;&#1072;&#1090;&#1072;)</P> <P LANG=\"zxx\" ALIGN=LEFT STYLE=\"text-indent: 0.21in; margin-bottom: 0in\"> <BR> </P> <P LANG=\"ru-RU\" ALIGN=LEFT STYLE=\"text-indent: -0.01in; margin-bottom: 0in; font-weight: normal; page-break-before: auto; page-break-after: auto\"> &#1054;&#1090; &#1101;&#1082;&#1089;&#1087;&#1083;&#1091;&#1072;&#1090;&#1080;&#1088;&#1091;&#1102;&#1097;&#1077;&#1081; &#1086;&#1088;&#1075;&#1072;&#1085;&#1080;&#1079;&#1072;&#1094;&#1080;&#1080; <SPAN LANG=\"en-US\"><B>"+"("+mOrg2spin+" ЛПУ МГ "+mOrg1spin+")"+"</B></SPAN></P> <P LANG=\"en-US\" ALIGN=LEFT STYLE=\"margin-bottom: 0in; font-weight: normal; page-break-before: auto; page-break-after: auto\"> ________________ ______________________________ __________ ________________</P> <P LANG=\"en-US\" ALIGN=LEFT STYLE=\"text-indent: 0.21in; margin-bottom: 0in; font-weight: normal\"> (&#1076;&#1086;&#1083;&#1078;&#1085;&#1086;&#1089;&#1090;&#1100;)&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;(&#1060;.&#1048;.&#1054;.)&emsp;&emsp;&emsp;&emsp;&emsp;(&#1087;&#1086;&#1076;&#1087;&#1080;&#1089;&#1100;)&emsp;&emsp;&emsp;(&#1076;&#1072;&#1090;&#1072;)</P> <P LANG=\"en-US\" ALIGN=LEFT STYLE=\"margin-bottom: 0in; font-weight: normal; page-break-before: auto; page-break-after: auto\"> ________________ ______________________________ __________ ________________</P> <P LANG=\"en-US\" ALIGN=LEFT STYLE=\"text-indent: 0.21in; margin-bottom: 0in; font-weight: normal\"> (&#1076;&#1086;&#1083;&#1078;&#1085;&#1086;&#1089;&#1090;&#1100;)&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;(&#1060;.&#1048;.&#1054;.)&emsp;&emsp;&emsp;&emsp;&emsp;(&#1087;&#1086;&#1076;&#1087;&#1080;&#1089;&#1100;)&emsp;&emsp;&emsp;(&#1076;&#1072;&#1090;&#1072;)</P>  <p>"+photocomment+"</p> <img src=\"data:image/jpeg;base64," + image64 + "\" /> </BODY> </HTML> ");

        startActivity(intent);
    }


    public void loadImageClick(View view) {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, GALLERY_REQUEST);
    }

    //метод для загрузкии изображения
    static final int GALLERY_REQUEST = 1;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

        Bitmap bitmap = null;
        ImageView imageViewLoad = (ImageView) findViewById(R.id.imageViewLoad);

        switch (requestCode) {
            case GALLERY_REQUEST:
                if (resultCode == RESULT_OK) {
                    Uri selectedImage = imageReturnedIntent.getData();
                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), selectedImage);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    imageViewLoad.setImageBitmap(bitmap);
                }
        }
    }

    /**
     * reduces the size of the image
     * @param image
     * @param maxSize
     * @return
     */
    public static Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float)width / (float) height;
        if (bitmapRatio > 0) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

}