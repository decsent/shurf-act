package dvinc.shurfact.data;

/**
 * Created by Space 5 on 29.11.2016.
 */

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.util.Log;

import dvinc.shurfact.data.ActContract.ActEntry;

/**
 * {@link ContentProvider} for Acts app.
 */
public class ActProvider extends ContentProvider {

    /** Tag for the log messages */
    public static final String LOG_TAG = ActProvider.class.getSimpleName();

    /** URI matcher code for the content URI for the acts table */
    private static final int ACTS = 100;

    /** URI matcher code for the content URI for a single act in the acts table */
    private static final int ACT_ID = 101;

    /**
     * UriMatcher object to match a content URI to a corresponding code.
     * The input passed into the constructor represents the code to return for the root URI.
     * It's common to use NO_MATCH as the input for this case.
     */
    private static final UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    // Static initializer. This is run the first time anything is called from this class.
    static {
        // The calls to addURI() go here, for all of the content URI patterns that the provider
        // should recognize. All paths added to the UriMatcher have a corresponding code to return
        // when a match is found.

        // The content URI of the form "content://com.example.android.acts/acts" will map to the
        // integer code {@link #actS}. This URI is used to provide access to MULTIPLE rows
        // of the acts table.
        sUriMatcher.addURI(ActContract.CONTENT_AUTHORITY, ActContract.PATH_ACTS, ACTS);

        // The content URI of the form "content://com.example.android.acts/acts/#" will map to the
        // integer code {@link #act_ID}. This URI is used to provide access to ONE single row
        // of the acts table.
        //
        // In this case, the "#" wildcard is used where "#" can be substituted for an integer.
        // For example, "content://com.example.android.acts/acts/3" matches, but
        // "content://com.example.android.acts/acts" (without a number at the end) doesn't match.
        sUriMatcher.addURI(ActContract.CONTENT_AUTHORITY, ActContract.PATH_ACTS + "/#", ACT_ID);
    }

    /** Database helper object */
    private ActDbHelper mDbHelper;

    @Override
    public boolean onCreate() {
        mDbHelper = new ActDbHelper(getContext());
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs,
                        String sortOrder) {
        // Get readable database
        SQLiteDatabase database = mDbHelper.getReadableDatabase();

        // This cursor will hold the result of the query
        Cursor cursor;

        // Figure out if the URI matcher can match the URI to a specific code
        int match = sUriMatcher.match(uri);
        switch (match) {
            case ACTS:
                // For the actS code, query the acts table directly with the given
                // projection, selection, selection arguments, and sort order. The cursor
                // could contain multiple rows of the acts table.
                cursor = database.query(ActEntry.TABLE_NAME, projection, selection, selectionArgs,
                        null, null, sortOrder);
                break;
            case ACT_ID:
                // For the act_ID code, extract out the ID from the URI.
                // For an example URI such as "content://com.example.android.acts/acts/3",
                // the selection will be "_id=?" and the selection argument will be a
                // String array containing the actual ID of 3 in this case.
                //
                // For every "?" in the selection, we need to have an element in the selection
                // arguments that will fill in the "?". Since we have 1 question mark in the
                // selection, we have 1 String in the selection arguments' String array.
                selection = ActEntry._ID + "=?";
                selectionArgs = new String[] { String.valueOf(ContentUris.parseId(uri)) };

                // This will perform a query on the acts table where the _id equals 3 to return a
                // Cursor containing that row of the table.
                cursor = database.query(ActEntry.TABLE_NAME, projection, selection, selectionArgs,
                        null, null, sortOrder);
                break;
            default:
                throw new IllegalArgumentException("Cannot query unknown URI " + uri);
        }
        // Set notification URI on the Cursor,
        // so we know what content URI the Cursor was created for.
        // If the data at this URI changes, then we know we need to update the Cursor.
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {
        final int match = sUriMatcher.match(uri);
        switch (match) {
            case ACTS:
                return insertAct(uri, contentValues);
            default:
                throw new IllegalArgumentException("Insertion is not supported for " + uri);
        }
    }

    /**
     * Insert a act into the database with the given content values. Return the new content URI
     * for that specific row in the database.
     */
    private Uri insertAct(Uri uri, ContentValues values) {

        //Здесь были проверки, или ещё будут...

        // Get writeable database
        SQLiteDatabase database = mDbHelper.getWritableDatabase();

        // Insert the new act with the given values
        long id = database.insert(ActEntry.TABLE_NAME, null, values);
        // If the ID is -1, then the insertion failed. Log an error and return null.
        if (id == -1) {
            Log.e(LOG_TAG, "Failed to insert row for " + uri);
            return null;
        }

        // Notify all listeners that the data has changed for the act content URI
        getContext().getContentResolver().notifyChange(uri, null);

        // Return the new URI with the ID (of the newly inserted row) appended at the end
        return ContentUris.withAppendedId(uri, id);
    }

    @Override
    public int update(Uri uri, ContentValues contentValues, String selection,
                      String[] selectionArgs) {
        final int match = sUriMatcher.match(uri);
        switch (match) {
            case ACTS:
                return updateAct(uri, contentValues, selection, selectionArgs);
            case ACT_ID:
                // For the ACT_ID code, extract out the ID from the URI,
                // so we know which row to update. Selection will be "_id=?" and selection
                // arguments will be a String array containing the actual ID.
                selection = ActEntry._ID + "=?";
                selectionArgs = new String[] { String.valueOf(ContentUris.parseId(uri)) };
                return updateAct(uri, contentValues, selection, selectionArgs);
            default:
                throw new IllegalArgumentException("Update is not supported for " + uri);
        }
    }

    /**
     * Update acts in the database with the given content values. Apply the changes to the rows
     * specified in the selection and selection arguments (which could be 0 or 1 or more acts).
     * Return the number of rows that were successfully updated.
     */
    private int updateAct(Uri uri, ContentValues values, String selection, String[] selectionArgs) {

        // check that the numb value is not null.
        if (values.containsKey(ActEntry.COLUMN_1_NUBM)) {
            String numb = values.getAsString(ActEntry.COLUMN_1_NUBM);
            if (numb == null) {
                throw new IllegalArgumentException("Act requires a number");
            }
        }

        if (values.containsKey(ActEntry.COLUMN_1_DATE)) {
            String date = values.getAsString(ActEntry.COLUMN_1_DATE);
            if (date == null) {
                throw new IllegalArgumentException("Act requires valid date");
            }
        }

        if (values.containsKey(ActEntry.COLUMN_1_OBJECT_NAME)) {
            String objname = values.getAsString(ActEntry.COLUMN_1_OBJECT_NAME);
            if (objname == null) {
                throw new IllegalArgumentException("Act requires valid object name");
            }
        }


        if (values.containsKey(ActEntry.COLUMN_1_COORD_PLACE)) {
            String coordpl = values.getAsString(ActEntry.COLUMN_1_COORD_PLACE);
            if (coordpl == null) {
                throw new IllegalArgumentException("Act requires valid coordpl");
            }

            // If there are no values to update, then don't try to update the database
            if (values.size() == 0) {
                return 0;
            }

            // Otherwise, get writeable database to update the data
            SQLiteDatabase database = mDbHelper.getWritableDatabase();

            // Perform the update on the database and get the number of rows affected
            int rowsUpdated = database.update(ActEntry.TABLE_NAME, values, selection, selectionArgs);

            // If 1 or more rows were updated, then notify all listeners that the data at the
            // given URI has changed
            if (rowsUpdated != 0) {
                getContext().getContentResolver().notifyChange(uri, null);
            }

            // Return the number of rows updated
            return rowsUpdated;
        }

        // No need to check the breed, any value is valid (including null).

        // If there are no values to update, then don't try to update the database
        if (values.size() == 0) {
            return 0;
        }

        // Otherwise, get writeable database to update the data
        SQLiteDatabase database = mDbHelper.getWritableDatabase();

        // Returns the number of database rows affected by the update statement
        return database.update(ActEntry.TABLE_NAME, values, selection, selectionArgs);
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        // Get writeable database
        SQLiteDatabase database = mDbHelper.getWritableDatabase();

        // Track the number of rows that were deleted
        int rowsDeleted;

        final int match = sUriMatcher.match(uri);
        switch (match) {
            case ACTS:
                // Delete all rows that match the selection and selection args
                rowsDeleted = database.delete(ActEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case ACT_ID:
                // Delete a single row given by the ID in the URI
                selection = ActEntry._ID + "=?";
                selectionArgs = new String[] { String.valueOf(ContentUris.parseId(uri)) };
                rowsDeleted = database.delete(ActEntry.TABLE_NAME, selection, selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Deletion is not supported for " + uri);
        }


        // If 1 or more rows were deleted, then notify all listeners that the data at the
        // given URI has changed
        if (rowsDeleted != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }

        // Return the number of rows deleted
        return rowsDeleted;
    }

    @Override
    public String getType(Uri uri) {
        final int match = sUriMatcher.match(uri);
        switch (match) {
            case ACTS:
                return ActEntry.CONTENT_LIST_TYPE;
            case ACT_ID:
                return ActEntry.CONTENT_ITEM_TYPE;
            default:
                throw new IllegalStateException("Unknown URI " + uri + " with match " + match);
        }
    }
}