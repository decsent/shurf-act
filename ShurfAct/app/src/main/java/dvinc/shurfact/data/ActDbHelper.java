package dvinc.shurfact.data;

/**
 * Created by Space 5 on 29.11.2016.
 */

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import dvinc.shurfact.data.ActContract.ActEntry;

/**
 * Database helper for ACTs app. Manages database creation and version management.
 */
public class ActDbHelper extends SQLiteOpenHelper {

    public static final String LOG_TAG = ActDbHelper.class.getSimpleName();

    /** Name of the database file */
    private static final String DATABASE_NAME = "shufract.db";

    /**
     * Database version. If you change the database schema, you must increment the database version.
     */
    private static final int DATABASE_VERSION = 1;

    /**
     * Constructs a new instance of {@link ActDbHelper}.
     *
     * @param context of the app
     */
    public ActDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /**
     * This is called when the database is created for the first time.
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        // Create a String that contains the SQL statement to create the acts table
        String SQL_CREATE_ACTS_TABLE =  "CREATE TABLE " + ActEntry.TABLE_NAME + " ("
                + ActEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + ActEntry.COLUMN_1_NUBM + " INTEGER, "
                + ActEntry.COLUMN_1_DATE + " TEXT, "
                + ActEntry.COLUMN_1_OBJECT_NAME + " TEXT, "
                + ActEntry.COLUMN_1_COORD_PLACE + " TEXT, "
                + ActEntry.COLUMN_1_LENGHT_PIT + " TEXT, "
                + ActEntry.COLUMN_1_GPS + " TEXT, "
                + ActEntry.COLUMN_1_OUTSIDE_DIAM + " TEXT, "
                + ActEntry.COLUMN_1_NOM_WALL_THICKNESS + " TEXT, "
                + ActEntry.COLUMN_1_BASE_FOR_PIT + " TEXT, "
                + ActEntry.COLUMN_1_DESC_TERR + " TEXT, "
                + ActEntry.COLUMN_GROUND_DEPTH + " TEXT, "
                + ActEntry.COLUMN_GROUND_TYPE + " TEXT, "
                + ActEntry.COLUMN_GROUND_GRUNT + " TEXT, "
                + ActEntry.COLUMN_GROUND_RESIST + " TEXT, "
                + ActEntry.COLUMN_GROUND_CONDIT + " TEXT, "
                + ActEntry.COLUMN_PROTEC_MATER + " TEXT, "
                + ActEntry.COLUMN_PROTEC_AVAILAB + " TEXT, "
                + ActEntry.COLUMN_PROTEC_CONDIT + " TEXT, "
                + ActEntry.COLUMN_PROTEC_THICKNESS+ " TEXT, "
                + ActEntry.COLUMN_ADGESION + " TEXT, "
                + ActEntry.COLUMN_DAMAGE_TYPE + " TEXT, "
                + ActEntry.COLUMN_DAMAGE_AVAILAB + " TEXT, "
                + ActEntry.COLUMN_DAMAGE_AREA + " TEXT, "
                + ActEntry.COLUMN_CORR_DAMAGE + " TEXT, "
                + ActEntry.COLUMN_CORR_DAM_CHAR + " TEXT, "
                + ActEntry.COLUMN_CORR_DAM_MAX + " TEXT, "
                + ActEntry.COLUMN_CORR_DAM_1+ " TEXT, "
                + ActEntry.COLUMN_CORR_DAM_1_3 + " TEXT, "
                + ActEntry.COLUMN_CORR_DAM_3 + " TEXT, "
                + ActEntry.COLUMN_OTHER_POTEN + " TEXT, "
                + ActEntry.COLUMN_OTHER_MATER + " TEXT, "
                + ActEntry.COLUMN_IMAGE + " TEXT, "
                + ActEntry.COLUMN_PHOTOCOMMENT + " TEXT, "
                + ActEntry.COLUMN_ORGANIZATION_1 + " TEXT, "
                + ActEntry.COLUMN_ORGANIZATION_2 + " TEXT, "
                + ActEntry.COLUMN_ORGANIZATION_USER + " TEXT);";


        // Execute the SQL statement
        db.execSQL(SQL_CREATE_ACTS_TABLE);
    }

    /**
     * This is called when the database needs to be upgraded.
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // The database is still at version 1, so there's nothing to do be done here.
    }
}