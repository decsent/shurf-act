package dvinc.shurfact.data;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by Space 5 on 29.11.2016.
 */

public class ActContract {

    // To prevent someone from accidentally instantiating the contract class,
    // give it an empty constructor.
    private ActContract() {}

    /**
     * The "Content authority" is a name for the entire content provider, similar to the
     * relationship between a domain name and its website.  A convenient string to use for the
     * content authority is the package name for the app, which is guaranteed to be unique on the
     * device.
     */
    public static final String CONTENT_AUTHORITY = "dvinc.shurfact";

    /**
     * Use CONTENT_AUTHORITY to create the base of all URI's which apps will use to contact
     * the content provider.
     */
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    /**
     * Possible path (appended to base content URI for possible URI's)
     * For instance, content://com.example.android.acts/acts/ is a valid path for
     * looking at act data. content://com.example.android.acts/staff/ will fail,
     * as the ContentProvider hasn't been given any information on what to do with "staff".
     */
    public static final String PATH_ACTS = "acts";

    /**
     * Inner class that defines constant values for the acts database table.
     * Each entry in the table represents a single act.
     */
    public static final class ActEntry implements BaseColumns {

        /** The content URI to access the act data in the provider */
        public static final Uri CONTENT_URI = Uri.withAppendedPath(BASE_CONTENT_URI, PATH_ACTS);

        /**
         * The MIME type of the {@link #CONTENT_URI} for a list of acts.
         */
        public static final String CONTENT_LIST_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_ACTS;

        /**
         * The MIME type of the {@link #CONTENT_URI} for a single act.
         */
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_ACTS;

        /** Name of database table for acts */
        public final static String TABLE_NAME = "acts";

        /**
         * Unique ID number for the act (only for use in the database table).
         *
         * Type: INTEGER
         */
        public final static String _ID = BaseColumns._ID;

        public final static String COLUMN_1_NUBM ="nubmer";
        public final static String COLUMN_1_DATE = "date";
        public final static String COLUMN_1_OBJECT_NAME = "object";
        public final static String COLUMN_1_COORD_PLACE = "coordplace";
        public final static String COLUMN_1_LENGHT_PIT = "lenghtpit";
        public final static String COLUMN_1_GPS = "gps";
        public final static String COLUMN_1_OUTSIDE_DIAM = "outsidediam";
        public final static String COLUMN_1_NOM_WALL_THICKNESS = "nomwall";
        public final static String COLUMN_1_BASE_FOR_PIT = "baseforpit";
        public final static String COLUMN_1_DESC_TERR = "descterr";
        public final static String COLUMN_GROUND_DEPTH = "depth";
        public final static String COLUMN_GROUND_TYPE = "groundtype";
        public final static String COLUMN_GROUND_GRUNT = "groundgrunt";
        public final static String COLUMN_GROUND_RESIST= "groundresist";
        public final static String COLUMN_GROUND_CONDIT = "groundcondit";
        public final static String COLUMN_PROTEC_MATER = "protecmatec";
        public final static String COLUMN_PROTEC_AVAILAB = "protecavailab";
        public final static String COLUMN_PROTEC_CONDIT = "proteccondit";
        public final static String COLUMN_PROTEC_THICKNESS = "protecthickness";
        public final static String COLUMN_ADGESION = "adgesion";
        public final static String COLUMN_DAMAGE_TYPE= "damagetype";
        public final static String COLUMN_DAMAGE_AVAILAB = "damageavailab";
        public final static String COLUMN_DAMAGE_AREA = "damagearea";
        public final static String COLUMN_CORR_DAMAGE = "corrdamage";
        public final static String COLUMN_CORR_DAM_CHAR = "corrdamchar";
        public final static String COLUMN_CORR_DAM_MAX = "corrdammax";
        public final static String COLUMN_CORR_DAM_1 = "corrdam1";
        public final static String COLUMN_CORR_DAM_1_3 = "corrdam13";
        public final static String COLUMN_CORR_DAM_3 = "corrdam3";
        public final static String COLUMN_OTHER_POTEN = "otherpoten";
        public final static String COLUMN_OTHER_MATER = "othermater";
        public final static String COLUMN_IMAGE = "image";
        public final static String COLUMN_PHOTOCOMMENT = "photocomment";
        public final static String COLUMN_ORGANIZATION_1 = "organization1";
        public final static String COLUMN_ORGANIZATION_2 = "organization2";
        public final static String COLUMN_ORGANIZATION_USER = "organizationuser";


        public static final String L_0 = "-";
        public static final String L_05 = "0,5";
        public static final String L_1 = "1";
        public static final String L_2 = "2";
        public static final String L_3 = "3";
        public static final String L_4 = "4";
        public static final String L_5 = "5";
        public static final String L_6 = "6";
        public static final String L_7 = "7";
        public static final String L_8 = "8";
        public static final String L_9 = "9";
        public static final String L_10 = "10";

        public static final String EMPTY_FIELD = " ";

        public static final String D_1 = "1420";
        public static final String D_2 = "1220";
        public static final String D_3 = "1020";
        public static final String D_4 = "820";
        public static final String D_5 = "720";
        public static final String D_6 = "530";
        public static final String D_7 = "426";
        public static final String D_8 = "325";
        public static final String D_9 = "273";
        public static final String D_10 = "219";
        public static final String D_11 = "159";
        public static final String D_12 = "108";

        public static final String R_1 = "ВТД";
        public static final String R_2 = "Экспертиза промышленной безопасности";
        public static final String R_3 = "Приемочное (первичное) обследование";
        public static final String R_4 = "Комплекное периодическое обследование";
        public static final String R_5 = "Детальное комплексное обследование";
        public static final String R_6 = "Инспекционно-техническое обследование";
        public static final String R_7 = "Специальное комплексное обследование";
        public static final String R_8 = "Техническое обслуживание";

        public static final String DT_1 = "Склон";
        public static final String DT_2 = "Овраг";
        public static final String DT_3 = "Равнина";
        public static final String DT_4 = "Степь";
        public static final String DT_5 = "Лес";
        public static final String DT_6 = "Просека";
        public static final String DT_7 = "Река";
        public static final String DT_8 = "Ручей";
        public static final String DT_9 = "Холмистая";

        public static final String GT_1 = "Глина";
        public static final String GT_2 = "Сухоглинок";
        public static final String GT_3 = "Супесь";
        public static final String GT_4 = "Песок";
        public static final String GT_5 = "Гумус";
        public static final String GT_6 = "Торф";
        public static final String GT_7 = "Известняк";
        public static final String GT_8 = "Гравий";
        public static final String GT_9 = "Галечник";
        public static final String GT_10 = "Чернозем";
        public static final String GTg_1 = "  ";
        public static final String GTg_2 = " с включением щебня";

        public static final String GC_1 = "Сухой";
        public static final String GC_2 = "Влажный";
        public static final String GC_3 = "Мокрый";

        public static final String PM_1 = "Полимерный";
        public static final String PM_2 = "Полиуретановый";
        public static final String PM_3 = "Эпоксидный";
        public static final String PM_4 = "Стеклоэмалевый";
        public static final String PM_5 = "Комбинированный";
        public static final String PM_6 = "Мастичный";
        public static final String PM_7 = "Термоусадочный";

        public static final String PA_1 = "Присутствует";
        public static final String PA_2 = "Отсутствует";

        public static final String PC_1 = ", Удовлетворительное";
        public static final String PC_2 = ", Неудовлетворительное";

        public static final String AD_1 = "Удовлетворительная";
        public static final String AD_2 = "Неудовлетворительная";

        public static final String DTy_1 = "Гофры";
        public static final String DTy_2 = "Складки";
        public static final String DTy_3 = "Пустоты";
        public static final String DTy_4 = "Механическме";

        public static final String DA_1 = "Да";
        public static final String DA_2 = "Нет";

        public static final String CD_1 = "Да";
        public static final String CD_2 = "Нет";

        public static final String CDC_1 = ", Пятнами";
        public static final String CDC_2 = ", Питтинговая";
        public static final String CDC_3 = ", Равномерная";

        public static final String CDM_1 = "1";
        public static final String CDM_2 = "2";
        public static final String CDM_3 = "3";
        public static final String CDM_4 = "4";
        public static final String CDM_5 = "5";
        public static final String CDM_6 = "6";
        public static final String CDM_7 = "7";

        public static final String CDV_1 = "1";
        public static final String CDV_2 = "2";
        public static final String CDV_3 = "3";
        public static final String CDV_4 = "4";
        public static final String CDV_5 = "5";
        public static final String CDV_6 = "6";
        public static final String CDV_7 = "7";
        public static final String CDV_8 = "8";
        public static final String CDV_9 = "9";
        public static final String CDV_10 = "10";
        public static final String CDV_11 = "11";
        public static final String CDV_12 = "12";
        public static final String CDV_13 = "13";
        public static final String CDV_14 = "14";
        public static final String CDV_15 = "15";
        public static final String CDV_16 = "16";
        public static final String CDV_17 = "17";
        public static final String CDV_18 = "18";
        public static final String CDV_19 = "19";
        public static final String CDV_20 = "20";

        public static final String orgN1 = "ООО \"Газпром трансгаз Беларусь\"";

        //---------------------До этого вроденорм, далее редакт

    }

}