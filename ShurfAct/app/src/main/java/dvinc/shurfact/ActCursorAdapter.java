package dvinc.shurfact;

/**
 * Created by Space 5 on 29.11.2016.
 */

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import dvinc.shurfact.data.ActContract;

/**
 * {@link ActCursorAdapter} is an adapter for a list or grid view
 * that uses a {@link Cursor} of act data as its data source. This adapter knows
 * how to create list items for each row of act data in the {@link Cursor}.
 */
public class ActCursorAdapter extends CursorAdapter {

    /**
     * Constructs a new {@link ActCursorAdapter}.
     *
     * @param context The context
     * @param c       The cursor from which to get the data.
     */
    public ActCursorAdapter(Context context, Cursor c) {
        super(context, c, 0 /* flags */);
    }

    /**
     * Makes a new blank list item view. No data is set (or bound) to the views yet.
     *
     * @param context app context
     * @param cursor  The cursor from which to get the data. The cursor is already
     *                moved to the correct position.
     * @param parent  The parent to which the new view is attached to
     * @return the newly created list item view.
     */
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.list_item, parent, false);
    }

    /**
     * This method binds the act data (in the current row pointed to by cursor) to the given
     * list item layout. For example, the name for the current act can be set on the name TextView
     * in the list item layout.
     *
     * @param view    Existing view, returned earlier by newView() method
     * @param context app context
     * @param cursor  The cursor from which to get the data. The cursor is already moved to the
     *                correct row.
     *
     *                ВНИЗУ МЕТОД ДЛЯ ЗАПОЛНЕНИЯ СПИСКА В СПИСКЕ АКТОВ, СОДЕРЖИТ НОМЕР И ДАТУ
     */
    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        // Find individual views that we want to modify in the list item layout
        TextView nameTextView = (TextView) view.findViewById(R.id.name);
        TextView summaryTextView = (TextView) view.findViewById(R.id.summary);

        // Find the columns of act attributes that we're interested in
        int nameColumnIndex = cursor.getColumnIndex(ActContract.ActEntry.COLUMN_1_NUBM);
        int dateColumnIndex = cursor.getColumnIndex(ActContract.ActEntry.COLUMN_1_DATE);

        // Read the act attributes from the Cursor for the current act
        int actNumb = cursor.getInt(nameColumnIndex);
        String date = cursor.getString(dateColumnIndex);

        // Update the TextViews with the attributes for the current act
        String nametext = "Акт № " + Integer.toString(actNumb) ;
        nameTextView.setText(nametext);
        summaryTextView.setText(date);
    }
}